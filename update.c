#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>

static char ubuf [10000000];
static char dbuf [10000000];
static char tmp [10000000];

int main (int argc, char * argv[]) {
  int ufd = open ("CodeROM.uasm", O_RDWR);
  if (ufd < 0) {
    perror ("open CodeROM.uasm");
    exit (1);
  }
  size_t usz = read (ufd, ubuf, sizeof (ubuf));
  fprintf (stderr, "CodeROM.uasm %ld\n", usz);
  ubuf[usz] = 0;

  int dfd = open ("disasm.dis", O_RDWR);
  if (dfd < 0) {
    perror ("open disasm.dis");
    exit (1);
  }
  size_t dsz = read (dfd, dbuf, sizeof (dbuf));
  fprintf (stderr, "disasm.dis %ld\n", dsz);
  dbuf[dsz] = 0;
  close (dfd);

  // Scan dis lookng for '@'

  char * dp = dbuf;
  char * atdp;
  char * bangdp;
  while ((atdp = strchr (dp, '@'))) {
    bangdp = strchr (atdp, '!');
    if (! bangdp) {
      fprintf (stderr, "no dis bang after %ld\n", atdp - dbuf);
      break;
    }
#if 0
    char * addrdp = atdp + 1;
    char * endptr;
    long addr = strtoul (addrdp, & endptr, 0);
#endif

    char addr[7];
    strncpy (addr, atdp, 6);
    addr[6] = 0;
    //printf ("%s\n", addr);

    // Scan CodeROM lookng for matching @addr
    char * atup;
    atup = strstr (ubuf, addr);
    if (atup) { // found
      char * bangup;
      bangup =  strchr (atup, '!');
      if (! bangup) {
        fprintf (stderr, "no uasm bang after %ld\n", atup - ubuf);
        break; // while atdp
      }
      // Copy everythng after the uasm bang to tmp
      strcpy (tmp, bangup + 1);
      // Nuke at the @
      * atup = 0;
      // stuff a temp null in dbuf
      char sv = bangdp[1];
      bangdp[1] = 0;
      // Append the dis string
      strcpy (atup, atdp);
      bangdp[1] = sv;
      // Append tmp back on
      strcat (atup, tmp);
    } else { // not found, append to the end
      // stuff a temp null in dbuf
      char sv = bangdp[1];
      bangdp[1] = 0;
      strcat (ubuf, atdp);
      bangdp[1] = sv;
      strcat (ubuf, "\n");
    }
    dp = bangdp + 1;
  } // while strchr (dp, '@')
  lseek (ufd, SEEK_SET, 0);
  write (ufd, ubuf, strlen (ubuf));
  ftruncate (ufd, strlen (ubuf));
  close (ufd);
}
