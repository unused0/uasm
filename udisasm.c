// Disassembler for Centurion CPU6 microcode

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <ctype.h>

static char * inputBuffer = NULL;
static size_t inputBufferLen= 0;
static int bits[56];
static int romAddr;

static void setbits2 (int i, int j) {
  int k = i * 4;
  bits[k + 0] = (j >> 3) & 1;
  bits[k + 1] = (j >> 2) & 1;
  bits[k + 2] = (j >> 1) & 1;
  bits[k + 3] = (j >> 0) & 1;
}

static void setbits (int i) {
  char ch = inputBuffer[i];
  if (ch >= '0' && ch <= '9')
    setbits2 (i, ch - '0');
  else if (ch >= 'a' && ch <= 'f')
    setbits2 (i, ch - 'a' + 10);
  else {
    printf ("<%o>\n", ch);
    exit (1);
  }
}

struct field {
  char * name;
  int start, end;
};
struct field fields[] = {
  { "INT_LVL",  55, 55 },  // Interrupt level
  { "SEQ_CTL0", 54, 54 },  // Sequencer control
  { "RLO",      53, 53 },  // Low byte for egister file access
  { "SFT_CRY",  52, 51 },  // Shift carry input control
  { "ARA_SEL",  50, 47 },  // ALU register A select
  { "ARB_SEL",  46, 43 },  // ALU register B select
  { "ALU_DST",  42, 40 },  // ALU destination
  { "ALU_SRC",  39, 34 },  // ALU source
  { "CND_ENB",  33, 33 },  // Conditional enable
  { "SEQ_CTL1", 32, 29 },  // Sequencer control
  { "STK_CTL",  28, 27 },  // Stack control
  { "SEQ_BITS", 26, 16 },  // Constant/Sequencer Direct Inputs/Conditional branch control/Condition Code Update
  { "CND_JSR",  15, 15 },  // Conditional JSR
  { "CNTRL0",   14, 13 },  // Control
  { "CNTRL1",   12, 10 },  // Control
  { "REG_LD0",   9,  7 },  // Register load
  { "REG_LD1",   6,  4 },  // Register load
  { "BUS_SEL",   3,  0 },  // Data bus select
  { NULL, 0, 0 }
};

static void hdr (void) {
  for (struct field * p = fields; p->name; p ++) {
    if (p->start == p-> end) {
      printf ("field %s %d\n", p->name, p->start);
    } else {
      printf ("field %s %d : %d\n", p->name, p->start, p->end);
    }
  }
}
  
static void dump (void) {
  for (struct field * p = fields; p->name; p ++) {
    printf ("%s <= 0b", p->name);
    for (int i = p->start; i >= p->end; i --)
      printf ("%d", bits[55 - i]);
    printf ("  ");
  }
  printf ("! ; %03x\n", romAddr);
}


int main (int argc, char * argv []) {
  hdr ();
  while (1) {
    int rc = getline (& inputBuffer, & inputBufferLen, stdin);
    if (rc == -1)
      break;
    //printf ("%s", inputBuffer);
    for (int i = 0; i < 14; i ++) 
      setbits (i);
    //for (int i = 0; i < 56; i ++)
      //printf ("%d", bits[i]);
    //printf ("\n");
    dump ();
    romAddr ++;
  }
}

