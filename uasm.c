// Reverse engineering tool for Centurion CPU6 microcode



#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include <fcntl.h>

#define ROM_WIDTH 56
#define ROM_LENGTH 2048

#define MAX_FIELDS 128
#define MAX_LABELS 4096
#define MAX_DEFS 4096
#define MAX_TARGET_ANNOTATIONS 4096
#define MAX_TARGETS 1024
#define DEFS_DEPTH 128
#define INCLUDE_DEPTH 8
#define INPUT_DEPTH 128 // 'include' and def expansion
#define MAX_SUB_FIELDS 64
#define MAX_TAGS 4096
#define MAX_NAME_LEN 128
#define NUM_GROUPS 32


#define IN_EXT ".uasm"
#define NEW_EXT ".new"
#define ROM_EXT ".rom"
#define LST_EXT ".lst"
#define DOC_EXT ".latex"

#define LABEL_MASK 0x7ff
#define SHORT_LABEL_MASK_LOW 0x00f
#define SHORT_LABEL_MASK_HIGH 0x7f0

#define tokeneq(a) (tokenLength == strlen (a) && strncmp (tokenStart, a, tokenLength) == 0)

#define NAME_SET "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz#$%+.-/_'`"

#ifdef WRITE_ROM
static FILE *fROM;
#endif
static FILE *fList;
static char *tokenPtr; // Where getToken is in the buffer
static char *tokenStart; // Start of token found by getToken
static size_t tokenLength; // Length of token found by getToken

static int romAddress;

// A data structure to hold a binary number from {0, 1, x}, where x is don't care or not set,
// depending on the context.
struct bitstring {
  uint8_t bits[ROM_WIDTH]; // 0 or 1
  bool set[ROM_WIDTH];    // x if false
};

#define newBitstring(foo) struct bitstring foo; memset (& foo, 0, sizeof foo);

// The words being assembled
static struct bitstring ROMWords[ROM_LENGTH];

//static bool isSequential[ROM_LENGTH];

//
// Fields
//

// A field of bits in a uinstruction
struct field {
  // The field name
  char fieldName[MAX_NAME_LEN];
  bool subfield;
  // The field is defined as a set of subfields
  int numSubFields;
  struct subField {
    // Each subfield runs from startBit:endBit, where startBit <= endBit.
    int startBit, endBit;
    // logically negate the bits in the ROM word
    bool invert;
  } subFields[MAX_SUB_FIELDS];
  // Number of bits set in all of the subfuelds.
  int bitCnt;
};
static struct field fields[MAX_FIELDS];
static int numFields = 0;

//
// Labels
//

struct label {
  char labelName[MAX_NAME_LEN];
  uint64_t value;
};
static struct label labels[MAX_LABELS];
static int numLabels = 0;

//
// Tags
//

struct tag {
  int fieldNumber; // index of parent field
  char tagName[MAX_NAME_LEN];
  //uint64_t tagValue;
  struct bitstring tagBits;
};
static struct tag tags[MAX_TAGS];
static int numTags;

//
// Defs
//

// defNormal  no parameters
// defDefault no parameters
// defOneParameter  a single numeric parameter
// defLabel  a single label parameter
// defLabelShort  a single label parameter of which the low 4 bits are used
// defLabelHigh  a single label parameter of which the high 7 bits are used
//   bits much match the current ROM address
enum defTypes { defNormal, defDefault, defOneParameter, defLabel, defLabelShort, defLabelHigh };
struct def {
  enum defTypes defType;
  char defName[MAX_NAME_LEN];
  char * defBody;
  struct bitstring defBits;
  struct bitstring query;
  int paramFieldIdx;
  bool default_;
  bool targetAnnotationP;
  int targetppAnnotationP;
  int group; // opcode ordering in the disassembly
};
static struct def defs[MAX_DEFS];
static int numDefs = 0;

// goup
// opcode ordering in the disassembly
static int currentGroup;

// targets
// A ucode instruction can jump to several different addresses in ways that are
// not easily derivable from the bit codes. Allow an annotation that lists 
// possible targets.

struct targetAnnotation {
  //uint64_t address;
  int numTargets;
  uint64_t targets[MAX_TARGETS];
};
static struct targetAnnotation targetAnnotations[ROM_LENGTH];

// usage table
// tag regions of microcode with labels tracking what instruction uses it.
static char usageMap[ROM_LENGTH][MAX_NAME_LEN];

// Include file stack

struct inputFile {
  FILE * inputFile;
  int lineNumber;
  char * inputBuffer;
  size_t inputBufferLength;
};
struct inputFile inputFileStack[INCLUDE_DEPTH];
static int inputFileStackPtr;
static char * srcFname = NULL;
static char * docFname = NULL;
#define FTOP inputFileStack[inputFileStackPtr]

// The actual CPU6 ROM contents
#include "CodeROM.h"


// Bit ordering
//
// CodeROM is an array of 56 bit integers.
// The MSB is bit 55, the LSB is bit 0.
// "bitstring" is an array of bits; bit 0 of a ROM word is in bitstring.bits[0].
// Note that reverses the order in memory; bitstring is stored LSB firt.
//

// toInt: Convert struct bitstring to uint64_t

static uint64_t toInt (struct bitstring * bits) {
  uint64_t r = 0;
  for (int n = 0; n < ROM_WIDTH; n ++)
    if (bits->set[n])
      r |= ((uint64_t) (bits->bits[n] & 01)) << n;
  return r;
}

// getBit: Get bit i from a uint64_t

static int getBit (uint64_t number, int i) {
  //uint64_t mask = ((uint64_t) 01) << (ROM_WIDTH - i - 1);
  uint64_t mask = ((uint64_t) 01) << (i);
  return (number & mask) ? (uint64_t) 1 : (uint64_t) 0;
}

// From uint64_t to bitstring

static void toBitstring (struct bitstring * bits, uint64_t value) {
  for (int bitNum = 0; bitNum < ROM_WIDTH; bitNum ++) {
    bits->set[bitNum] = true;
    bits->bits[bitNum] = getBit (value, bitNum);
  }
}

// change 11 digit binary digit string to hex string

static char * binToHex (char * work, char * in) {
#define V(i) (in[(i)] - '0')
  static char h[] = "0123456789abcdef";
  work[0] = h[           V(0) * 4 + V(1) * 2 + V(2)];  
  work[1] = h[V(3) * 8 + V(4) * 4 + V(5) * 2 + V(6)];  
  work[2] = h[V(7) * 8 + V(8) * 4 + V(9) * 2 + V(10)];  
  work[3] = 0;
//fprintf (stderr, "%s %s\n", in, work);
  return work;
}

// format a bitstring[nbits-1:0] as a string

static void bitstringToString (struct bitstring * value, int nbits, char buffer[ROM_WIDTH + 1]) {
  char * p = buffer;
  for (int i = nbits - 1; i >= 0; i --) {
    if (value->set[i])
      *p++ = value->bits[i] + '0';
    else
      *p++ = 'x';
  }
  *p++ = 0;
}


// format a uint64_t as binary

static void toBin (uint64_t number, char buffer[ROM_WIDTH + 1]) {
  for (int i = 0; i < ROM_WIDTH; i ++) {
     //uint64_t mask = ((uint64_t) 01) << (ROM_WIDTH - i - 1);
     //buffer[i] = (number & mask) ?  '1' : '0';
     buffer[i] = getBit (number, ROM_WIDTH - i - 1) + '0';
  }
  buffer[ROM_WIDTH] = 0;
}

static int numberErrors = 0;
static void errStr (char * fmt, char * p) {
  fprintf (stderr, "Line %d: ", FTOP.lineNumber);
  fprintf (stderr, fmt, p);
  fprintf (fList, fmt, p);
  numberErrors ++;
}
static void errIntUint64 (char * fmt, int p1, uint64_t p2) {
  fprintf (stderr, "Line %d: ", FTOP.lineNumber);
  fprintf (stderr, fmt, p1, p2);
  fprintf (fList, fmt, p1, p2);
  numberErrors ++;
}
static void errInt (char * fmt, int p) {
  fprintf (stderr, "Line %d: ", FTOP.lineNumber);
  fprintf (stderr, fmt, p);
  fprintf (fList, fmt, p);
  numberErrors ++;
}
static void errLong (char * fmt, long p) {
  fprintf (stderr, "Line %d: ", FTOP.lineNumber);
  fprintf (stderr, fmt, p);
  fprintf (fList, fmt, p);
  numberErrors ++;
}
static void errStrStr (char * fmt, char * p1, char * p2) {
  fprintf (stderr, "Line %d: ", FTOP.lineNumber);
  fprintf (stderr, fmt, p1, p2);
  fprintf (fList, fmt, p1, p2);
  numberErrors ++;
}
static void errU64SU64SS (char * fmt, uint64_t p1, char * p2, uint64_t p3, char * p4, char * p5) {
  fprintf (stderr, "Line %d: ", FTOP.lineNumber);
  fprintf (stderr, fmt, p1, p2, p3, p4, p5);
  fprintf (fList, fmt, p1, p2, p3, p4, p5);
  numberErrors ++;
}
static void err (char * fmt) {
  fprintf (stderr, "Line %d: ", FTOP.lineNumber);
  fprintf (stderr, fmt);
  fprintf (fList, fmt);
  numberErrors ++;
}

// imply struct bitstring

static void updateBits (struct bitstring * to, struct bitstring * from) {
  for (int bitNum = 0; bitNum < ROM_WIDTH; bitNum ++) {
    if (! from->set[bitNum])
      continue;
    if (to->set[bitNum] && (to->bits[bitNum] != from->bits[bitNum])) {
      errInt ("updateBits: bit %d already set\n", bitNum);
    }
    to->set[bitNum] = true;
    to->bits[bitNum] = from->bits[bitNum];
  }
}


static void updateBitsDefault (struct bitstring * to, struct bitstring * from) {
  for (int bitNum = 0; bitNum < ROM_WIDTH; bitNum ++) {
    if (! from->set[bitNum])
      continue;
    if (! to->set[bitNum]) {
      to->set[bitNum] = true;
      to->bits[bitNum] = from->bits[bitNum];
    }
  }
}


// Check for duplicate name

static bool dupCheck (int tagFieldNumber) {

  // field names
  for (int i = 0; i < numFields; i ++)
    if (tokeneq (fields[i].fieldName))
      return true;

  // label names
  for (int i = 0; i < numLabels; i ++)
    if (tokeneq (labels[i].labelName))
      return true;

  // def names
  for (int i = 0; i < numDefs; i ++)
    if (tokeneq (defs[i].defName))
      return true;

  // tags
  // if tagFieldNumber is >= 0, then the token as a tag name associated 
  // that field; only search that fields tags for dups.
  for (int i = 0; i < numTags; i ++) {
    if (tokeneq (tags[i].tagName) && (tagFieldNumber < 0 || tagFieldNumber == tags[i].fieldNumber))
      return true;
  }
  return false;
}
  

//static void brkbrk (void) {}

// Copy the set bits in a field to ROM

static void setROMBits (struct bitstring * bitstring) {
  for (int bitNum = 0; bitNum < ROM_WIDTH; bitNum ++) {
    if (! bitstring->set[bitNum])
      continue;
    if (ROMWords[romAddress].set[bitNum] && (ROMWords[romAddress].bits[bitNum] != bitstring->bits[bitNum])) {
      errInt ("bit %d already set in ROM\n", bitNum);
    }
    ROMWords[romAddress].bits[bitNum] = bitstring->bits[bitNum];
    ROMWords[romAddress].set[bitNum] = true;
  }
}



// Take the value of from, map it according to fldPtr and set bits in to

static void setBitsBits (struct bitstring * from, struct field * fldPtr, struct bitstring * to) {
  int bitsIndex = 0;  // start at LSB in from
  // From the last sub field to first (least significant subfield to most)
  for (int subFieldNumber = fldPtr->numSubFields - 1; subFieldNumber >= 0; subFieldNumber --) {
    struct subField * p = fldPtr->subFields + subFieldNumber;
    // Number of bits in the subfield
    int nbits = p->startBit - p->endBit + 1;
    for (int i = 0; i < nbits; i ++) {
      // From subfield LSB to MSB
      int bitNum = p->endBit + i;
      // Get bits from bits LSB to MSB
      uint64_t bitval = from->bits[bitsIndex];  // 1 or 0
      if (p -> invert)
         bitval ^= 01;
      // If false, digit was 'x'
      bool set = from->set[bitsIndex];
      bitsIndex ++;
      if (set) {
        if (to->set[bitNum]) {
          errInt ("bit %d already set\n", bitNum);
        }
        to->set[bitNum] = true;
        to->bits[bitNum] = bitval;
      }
    }
  }
}




static void setBitsFieldBitsValue (uint64_t value, struct field * fldPtr, struct bitstring * to) {
  newBitstring (bits);
  toBitstring (& bits, value);
  setBitsBits (& bits, fldPtr, to);
}


  

static bool bitsBitsMatch (struct bitstring * haystack, struct def * needle) {
  int nHits = 0;
  for (int bitNum = 0; bitNum < ROM_WIDTH; bitNum ++) {
    if (! needle->defBits.set[bitNum])
      continue;
// Set for loose fit
#if 0
    if ((! haystack->set[bitNum]) && (! needle->query.set[bitNum]))
      return false;
#endif
    if (needle->defBits.bits[bitNum] != haystack->bits[bitNum])
      return false;
    if (haystack->set[bitNum])
      nHits ++;
  }
  return nHits != 0;
}

static bool bitsBitsPartialMatch (struct bitstring * ROMWord, struct field * fp, struct bitstring * matchedBits, char number[ROM_WIDTH + 1], char numberx[ROM_WIDTH + 1], uint64_t * value) {
  char * np = number;
  *np = 0;
  char * nx = numberx;
  *nx = 0;

  *value = 0;

  int bitcnt = 0;
  int nHits = 0;
  memset (matchedBits, 0, sizeof (*matchedBits));
  for (int s = 0; s < fp->numSubFields; s ++) {
    struct subField * sfp = fp->subFields + s;
    for (int bitNum = sfp->startBit; bitNum >= sfp->endBit; bitNum --) {
      if (bitcnt >= ROM_WIDTH) {
        err ("getVal disBuf overflow; bailing.\n");
        return false;
      }
      *np = '0' + (ROMWord->bits[bitNum] ^ (sfp->invert ? 1 : 0));
      if (ROMWord->set[bitNum])
        *nx = *np;
      else
        *nx = 'x';
      *value = ((*value) << 1) + ROMWord->bits[bitNum];
      matchedBits->bits[bitNum] = ROMWord->bits[bitNum];
#if 0
      matchedBits->set[bitNum] = true;
#else
      matchedBits->set[bitNum] = ROMWord->set[bitNum];
#endif
      if (ROMWord->set[bitNum])
        nHits ++;
      np ++;
      * np = 0;
      nx ++;
      * nx = 0;
      bitcnt ++;
    }
  }
  return nHits != 0;
}



// parseNumber
//   0[xX]{hex_digits}   0123456789aAbBcCdDeEfFxX
//   0[bB]{binary_digits}  01xX
//

static bool parseNumber (uint64_t * value, int * noBits) {
  if (tokenStart[0] != '0')
    return false;
  if (tokenStart[1] == 'b' || tokenStart[1] == 'B') {
    * value = 0;
    * noBits = 0;
    for (int i = 2; i < tokenLength; i ++) {
      char ch = tokenStart[i];
      if (ch != '0' && ch != '1')
        return false;
      (* value) <<= 1;
      if (ch == '1')
        (* value) |= 01;
      (* noBits) ++;
    }   
    return true;
  }
  if (tokenStart[1] == 'x' || tokenStart[1] == 'X') {
    * value = 0;
    * noBits = 0;
    for (int i = 2; i < tokenLength; i ++) {
      char ch = tolower (tokenStart[i]);
      if (ch >= '0' && ch <= '9') {
        (* value) <<= 4;
        *value |= ch - '0';
        continue;
      }
      if (ch >= 'a' && ch <= 'f') {
        (* value) <<= 4;
        *value |= ch - 'a' + 10;
        continue;
      }
      return false;   
    }   
    return true;
  }
  return false;
}



// parseNumberBits
//   0[bB]{binary_digits x X}
//

static bool parseNumberBits (struct bitstring * value, int * numBits) {
  if (tokenStart[0] != '0')
    return false;
  memset (value, 0, sizeof (* value));
  * numBits = 0;
  if (tokenStart[1] == 'b' || tokenStart[1] == 'B') {
    if (tokenLength - 2 > ROM_WIDTH) {
      err ("Number too big; ignoring.\n");
    }
    // Length of number (tokenLength - 2) tells us where the MSB is
    int n = (tokenLength - 2) - 1;
    for (int i = 2; i < tokenLength; i ++) {
      char ch = tokenStart[i];
      if (ch == 'x' || ch == 'X') {
        // value->bits[n] = 0;
        // value->set[n] = false;
      } else if (ch == '0') {
        // value->bits[n] = 0;
         value->set[n] = true;
      } else if (ch == '1') {
        value->bits[n] = 1;
        value->set[n] = true;
      } else {
        return false;
      }
      n --;
      (* numBits) ++;
    }   
//char foo [tokenLength + 1];
//strncpy (foo, tokenStart, tokenLength);
//foo[tokenLength] = 0;
//char val[ROM_WIDTH + 1];
//bitstringToString (value, /* *numBits */ 56, val);
//fprintf (stderr, "b:      <%s> <%s>\n", foo, val);
    return true;
  }
  if (tokenStart[1] == 'x' || tokenStart[1] == 'X') {
    // Length of number (tokenLength - 2) tells us where the MS hex digit is
    int n = ((tokenLength - 2) * 4) - 1;
    if (n < 0) {
      err ("Number too big; ignoring.\n");
      return false;
    }
//fprintf (stderr, "n:  %d\n", n);
    for (int i = 2; i < tokenLength; i ++) {
      char ch = tokenStart[i];

      if (ch == 'x' || ch == 'X') {
        // value->bits[n] = 0;
        // value->set[n] = false;
      } else {
        unsigned int v;
        if (ch >= '0' && ch <= '9')
          v = ch - '0';
        else if (ch >= 'a' && ch <= 'f')
          v = ch - 'a' + 10;
        else if (ch >= 'A' && ch <= 'F')
          v = ch - 'A' + 10;
        else
          return false;
//fprintf (stderr, "v:  %x\n", v);
        value->bits[n - 0] = (v >> 3) & 1;
        value->set [n - 0] = true;
        value->bits[n - 1] = (v >> 2) & 1;
        value->set [n - 1] = true;
        value->bits[n - 2] = (v >> 1) & 1;
        value->set [n - 2] = true;
        value->bits[n - 3] = (v >> 0) & 1;
        value->set [n - 3] = true;
      }
      n -= 4;
      (* numBits) += 4;
    }   
//char foo [tokenLength + 1];
//strncpy (foo, tokenStart, tokenLength);
//foo[tokenLength] = 0;
//char val[ROM_WIDTH + 1];
//bitstringToString (value, /* *numBits */ 56, val);
//fprintf (stderr, "x:      <%s> <%s>\n", foo, val);
    return true;
  }
  return false;
}


static bool getToken (void) {
  while (*tokenPtr) {

    // eat leading spaces and newlines
    if (isspace (*tokenPtr) || iscntrl (*tokenPtr)) {
      tokenPtr ++;
      continue;
    }

    // ;
    if (*tokenPtr == ';') { // Semicolons are comments, discard to end of line
      while (*tokenPtr) {
        tokenPtr ++;
      }
      return false;
    }

    tokenStart =  tokenPtr;
    // Octal, hexadecimal or binary numbers start with 0
    if (*tokenPtr == '0') {
      tokenPtr ++;

      // Binary number?
      if (*tokenPtr == 'b' || *tokenPtr == 'B') {
        tokenPtr ++;
        tokenPtr += strspn (tokenPtr, "01xX");
        tokenLength = tokenPtr - tokenStart;
        return true;
      }

      // Hex number?
      if (*tokenPtr == 'x' || *tokenPtr == 'x') {
        tokenPtr ++;
        tokenPtr += strspn (tokenPtr, "0123456789abcdefABCDEFxX");
        tokenLength = tokenPtr - tokenStart;
        return true;
      }

      // Octal number
      tokenPtr += strspn (tokenPtr, "01234567xX");
      tokenLength = tokenPtr - tokenStart;
      return true;
    }

    // Decimal number
    if (isdigit (*tokenPtr)) {
      tokenPtr += strspn (tokenPtr, "0123456789xX");
      tokenLength = tokenPtr - tokenStart;
      return true;
    }

    // At sign
    if (*tokenPtr == '@') {
      tokenPtr ++;
      tokenLength = 1;
      return true;
    }

    // Colon
    if (*tokenPtr == ':') {
      tokenPtr ++;
      tokenLength = 1;
      return true;
    }

    // Exclamation point
    if (*tokenPtr == '!') {
      tokenPtr ++;
      tokenLength = 1;
      return true;
    }

    // Semicolon
    if (*tokenPtr == ';') {
      tokenPtr ++;
      tokenLength = 1;
      return true;
    }

    // tilde
    if (*tokenPtr == '~') {
      tokenPtr ++;
      tokenLength = 1;
      return true;
    }

    // <=
    if (*tokenPtr == '<' && tokenPtr[1] == '=') {
      tokenPtr += 2;
      tokenLength = 2;
      return true;
    }

    // ?=
    if (*tokenPtr == '?' && tokenPtr[1] == '=') {
      tokenPtr += 2;
      tokenLength = 2;
      return true;
    }

    // name
    size_t len = strspn (tokenPtr, NAME_SET);
    if (len) {
      tokenPtr += len;
      tokenLength = len;
      return true;
    }
   
    errInt ("Unexpected character o%03o; ignoring.\n", *tokenPtr);
    tokenPtr ++;
  }
  return false;
}


// Record the generated word in ROM and on the listing file
static bool doWrite (void) {

  for (int defNum = numDefs - 1; defNum >= 0; defNum --) {
    struct def * dp = defs + defNum;
    if (! dp->default_)
      continue;
    updateBitsDefault (& ROMWords[romAddress], & dp->defBits);
  } // for
  
  uint64_t word = toInt (& ROMWords[romAddress]);
  fprintf (fList, "    %03x %014lx\n", romAddress, word);
  if (word != CodeROM[romAddress]) {
    char got[ROM_WIDTH + 1];
    toBin (word, got);
    char exp[ROM_WIDTH + 1];
    toBin (CodeROM[romAddress], exp);
    char caret[ROM_WIDTH + 1];
    for (int i = 0; i < ROM_WIDTH; i ++)
      caret[i] = got[i] == exp[i] ? ' ' : '^';
    caret[ROM_WIDTH] = 0;
    errU64SU64SS ("CodeROM mismatch\n"
                 "                  got %014lx %s\n"
                 "           , expected %014lx %s\n"
                 "                                     %s\n"
                 "                                     55555544444444443333333333222222222211111111110000000000\n"
                 "                                     54321098765432109876543210987654321098765432109876543210\n",
                 word, got, CodeROM[romAddress], exp, caret);
  }
  for (int i = ROM_WIDTH - 1; i >= 0; i --) {
    if (! ROMWords[romAddress].set[i]) {
      errInt ("Word bit %d not set.\n", i);
    }
  }
  romAddress ++;
  return true;
}


static bool doLabel (void) {
  // Get label name
  if (! getToken ()) {
    err ("Unable to parse label name; ignoring.\n");
    return false;
  }
  if (tokenLength > MAX_NAME_LEN) {
    err ("Label name too long; ignoring.\n");
    return false;
  }
  char labelName[MAX_NAME_LEN];
  strncpy (labelName, tokenStart, tokenLength);
  labelName[tokenLength] = 0;

  // Look it up
  int labelNumber = 0;
  for (labelNumber = 0; labelNumber < numLabels; labelNumber ++) 
    if (strcmp (labelName, labels[labelNumber].labelName) == 0)
      break;
  // Not found
  if (labelNumber >= numLabels) {
    err ("Undefinded label\n");
    return false;
  }
  // At correct address?
  if (labels[labelNumber].value != romAddress) {
    errIntUint64 ("Label at address 0x%03x, expected 0x%03lx\n", romAddress, labels[labelNumber].value);
    return false;
  }
  return true;
}



static bool doLabelDef (bool usageP) {
  // Enough room?
  if (numLabels >= MAX_LABELS) {
    err ("Too many labels\n");
    return false;
  }
  // Get label name
  if (! getToken ()) {
    err ("Unable to parse label name; ignoring.\n");
    return false;
  }
  if (tokenLength > MAX_NAME_LEN) {
    err ("Label name too long; ignoring.\n");
    return false;
  }
  strncpy (labels[numLabels].labelName, tokenStart, tokenLength);
  labels[numLabels].labelName[tokenLength] = 0;

  // Duplicate?
  if (dupCheck (-1)) {
    err ("Duplicate label name; ignoring.\n");
    return false;
  }

  // Get label value
  if (! getToken ()) {
    err ("Unable to parse def named; ignoring.\n");
    return false;
  }

  int numBits;
  if (! parseNumber (& labels[numLabels].value, & numBits)) {
    err ("Unable to parse def named; ignoring.\n");
    return false;
  }

  if (usageP) {
    if (labels[numLabels].value >= ROM_LENGTH) {
      err ("Address too big; ignoring.\n");
    } else {
      strcpy (usageMap[labels[numLabels].value], labels[numLabels].labelName);
    }
  }

  // Add the label
  numLabels ++;

  return true;
}



// target address <tareget_address_list>

static bool doTarget (void) {
  // address
  uint64_t address;
  if (! getToken ()) {
    err ("Unable to parse target address; ignoring.\n");
    return false;
  }

  // Is value a label?
  int labelNumber;
  for (labelNumber = 0; labelNumber < numLabels; labelNumber ++)
    if (tokeneq (labels[labelNumber].labelName))
      break;
  // found?
  if (labelNumber < numLabels) {
    address = labels[labelNumber].value;
  } else {
    int numBits;
    // Is value a number?
    if (! parseNumber (& address, & numBits)) {
      err ("Unable to parse target address; ignoring.\n");
      return false;
    }
  }

  // Parse address list
  while (1) {
    // Enough room?
    if (targetAnnotations[address].numTargets >= MAX_TARGETS) {
      err ("Too many targets\n");
      return false;
    }
    if (! getToken ()) {
      break;
    }

    uint64_t targetAddress;
    // Is value a label?
    int labelNumber;
    for (labelNumber = 0; labelNumber < numLabels; labelNumber ++)
      if (tokeneq (labels[labelNumber].labelName))
        break;
    // found?
    if (labelNumber < numLabels) {
      targetAddress = labels[labelNumber].value;
    } else {
      int numBits;
      // Is value a number?
      if (! parseNumber (& targetAddress, & numBits)) {
        err ("Unable to parse target address; ignoring.\n");
        return false;
      }
    }
    targetAnnotations[address].targets[targetAnnotations[address].numTargets] = targetAddress;
    targetAnnotations[address].numTargets ++;
  }
  return true;
}



// usage address string

static bool doUsage (void) {
  // address
  uint64_t address;
  if (! getToken ()) {
    err ("Unable to parse target address; ignoring.\n");
    return false;
  }

  // Is value a label?
  int labelNumber;
  for (labelNumber = 0; labelNumber < numLabels; labelNumber ++)
    if (tokeneq (labels[labelNumber].labelName))
      break;
  // found?
  if (labelNumber < numLabels) {
    address = labels[labelNumber].value;
  } else {
    int numBits;
    // Is value a number?
    if (! parseNumber (& address, & numBits)) {
      err ("Unable to parse target address; ignoring.\n");
      return false;
    }
  }
  if (address >= ROM_LENGTH) {
    err ("Address too big; ignoring.\n");
    return false;
  }

  // Get string
  if (! getToken ()) {
    err ("Unable to parse usage string; ignoring.\n");
    return false;
  }
  if (tokenLength > MAX_NAME_LEN) {
    err ("usage string too long; ignoring.\n");
    return false;
  }
  char string [tokenLength + 1];
  strncpy (string, tokenStart, tokenLength);
  string[tokenLength] = 0;

  strcpy (usageMap[address], string);
  return true;
}


static bool doOpLabelDef (void) {
  // Enough room?
  if (numLabels >= MAX_LABELS) {
    err ("Too many labels\n");
    return false;
  }
  // Get label name
  if (! getToken ()) {
    err ("Unable to parse label name; ignoring.\n");
    return false;
  }
  if (tokenLength > MAX_NAME_LEN) {
    err ("Label name too long; ignoring.\n");
    return false;
  }
  strncpy (labels[numLabels].labelName, tokenStart, tokenLength);
  labels[numLabels].labelName[tokenLength] = 0;

  // Duplicate?
  if (dupCheck (-1)) {
    err ("Duplicate label name; ignoring.\n");
    return false;
  }

  // Get offset
  if (! getToken ()) {
    err ("Unable to parse def named; ignoring.\n");
    return false;
  }

  int numBits;
  uint64_t offset;

  if (! parseNumber (& offset, & numBits)) {
    err ("Unable to parse def named; ignoring.\n");
    return false;
  }

  if (offset > 0xff) {
    err ("Offset > 0xff; ignoring.\n");
    return false;
  }

  // Get value
  if (! getToken ()) {
    err ("Unable to parse def named; ignoring.\n");
    return false;
  }

  uint64_t value;

  if (! parseNumber (& value, & numBits)) {
    err ("Unable to parse def named; ignoring.\n");
    return false;
  }

  if ((value & 0xff) != CPU_6309[offset]) {
    char buf[4096];
    sprintf (buf, "6309 PROM got %02lx, expected %02x\n", value & 0xff, CPU_6309[offset]);
  }

  //labels[numLabels].value = 0x100 | (uint64_t) CPU_6309[offset];
  labels[numLabels].value = value;

//  strcpy (usageMap[labels[numLabels].value], labels[numLabels].labelName);

  // Add the label
  numLabels ++;

  return true;
}




// Read def name and body
// def name body....
// def1 name param body
static bool doDefDef (enum defTypes defType) {

  // Get def name
  if (! getToken ()) {
    err ("Unable to parse def name; ignoring.\n");
    return false;
  }
  if (tokenLength > MAX_NAME_LEN) {
    err ("Def name too long; ignoring.\n");
    return false;
  }
  char defName[MAX_NAME_LEN];
  strncpy (defName, tokenStart, tokenLength);
  defName[tokenLength] = 0;

  // Room?
  if (numDefs >= MAX_DEFS) {
    err ("Too many defs\n");
    return false;
  }
  // Check for duplicate name
  if (dupCheck (-1)) {
    err ("Duplicate def name; ignoring.\n");
    return false;
  }

  struct def * defPtr = defs + numDefs;

  // Add to definitons
  strcpy (defPtr->defName, defName);
  defPtr->defType = defType;
  defPtr->default_ = defType == defDefault;
  defPtr->group = currentGroup;
  // def body in the rest of buffer
  defPtr->defBody = strdup (tokenPtr);
  // Replace newlines with spaces; tidies the listing.
  for (char * p = defPtr->defBody; *p; p ++)
     if (*p == '\n')
       *p = ' ';
  memset (& defPtr->defBits, 0, sizeof (defPtr->defBits));
  memset (& defPtr->query, 0, sizeof (defPtr->query));
  defPtr->targetAnnotationP = false;
  defPtr->targetppAnnotationP = false;
  defPtr->paramFieldIdx = -1;
  if (defType == defOneParameter || defType == defLabel || defType == defLabelShort || defType == defLabelHigh) {
    // Get the parameter field name
    if (! getToken ()) {
      err ("Unable to parse def parameter name; ignoring.\n");
      return false;
    }
    if (tokenLength > MAX_NAME_LEN) {
      err ("Def parameter name too long; ignoring.\n");
      return false;
    }
    char paramName[MAX_NAME_LEN];
    strncpy (paramName, tokenStart, tokenLength);
    paramName[tokenLength] = 0;

    // Lookup the parameter 
    int fieldIdx;
    for (fieldIdx = 0; fieldIdx < numFields; fieldIdx ++)
      if (strcmp (paramName, fields[fieldIdx].fieldName) == 0)
        break; 
    if (fieldIdx >= numFields) {
      err ("Def parameter not defined; ignoring.\n");
      return false;
    } else {
      defPtr->paramFieldIdx = fieldIdx;
    }
  }

  // Parse the body
  while (getToken ()) {
    // field name?
    int fieldNumber;
    for (fieldNumber = 0; fieldNumber < numFields; fieldNumber ++) {
      if (tokeneq (fields[fieldNumber].fieldName)) {
        break;
      }
    }
    // found?
    if (fieldNumber < numFields) {
      struct field * fldPtr = fields + fieldNumber;

      // <=
      if (! getToken ()) {
        err ("Unable to parse field reference operaror; ignoring.\n");
        return false;
      }
      if ((! tokeneq ("<=")) && (! tokeneq ("?="))) {
        err ("Unable to parse field reference operaror; ignoring.\n");
        return false;
      }
      bool query = tokeneq ("?=");

      // value
      if (! getToken ()) {
        err ("Unable to parse field value; ignoring.\n");
        return false;
      }

      int numBits;

      // Is value a label?
      int labelNumber;
      for (labelNumber = 0; labelNumber < numLabels; labelNumber ++) {
        if (tokeneq (labels[labelNumber].labelName)) {
          break;
        }
      }
      // found?
      if (labelNumber < numLabels) {
        uint16_t value = labels[labelNumber].value;
        newBitstring (bits);
        setBitsFieldBitsValue (value, fldPtr, & bits);
        if (query)
          updateBits (& defPtr->query, & bits);
        updateBits (& defPtr->defBits, & bits);
        continue;
      }


      // Is value a tag?
      int tagNumber;
      for (tagNumber = 0; tagNumber < numTags; tagNumber ++)
        if (tokeneq (tags[tagNumber].tagName) && fieldNumber == tags[tagNumber].fieldNumber)
          break;
      // found?
      if (tagNumber < numTags) {
        struct bitstring * bitPtr = & tags[tagNumber].tagBits;
        if (query)
          updateBits (& defPtr->query, bitPtr);
        updateBits (& defPtr->defBits, bitPtr);
        continue;
      } 

      // Is value a number?
      {
        newBitstring (valueBits);
        if (parseNumberBits (& valueBits, & numBits)) {
          newBitstring (bits);
          setBitsBits (& valueBits, fldPtr, & bits);
          if (query)
            updateBits (& defPtr->query, & bits);
          updateBits (& defPtr->defBits, & bits);
          continue;
        }
      }
      continue;
    }

    // Is the token a def ref?
    int refDefNumber;
    for (refDefNumber = 0; refDefNumber < numDefs; refDefNumber ++) {
      if (tokeneq (defs[refDefNumber].defName)) {
        break;
      }
    }
    // found?
    if (refDefNumber < numDefs) {
      updateBits (& defPtr->query, & defs[refDefNumber].defBits);
      updateBits (& defPtr->defBits, & defs[refDefNumber].defBits);
      // Does the referenced def have a parameter?
      if (defs[refDefNumber].paramFieldIdx >= 0) {
        // Does the def being defined have a parameter?
        if (defPtr->paramFieldIdx >= 0) {
          err ("Both defined and referenced defs have a paramter; ignoring.\n");
          return false;
        }
        // Inherit the parameter
        defPtr->paramFieldIdx = defs[refDefNumber].paramFieldIdx;
      }
      continue;
    }

    // Is the token a keyword?
    if (tokeneq ("__target")) {
      defPtr->targetAnnotationP = true;
      continue;
    }
    if (tokeneq ("__target++")) {
      defPtr->targetppAnnotationP = true;
      continue;
    }

    // Unrecognized token
    char foo [tokenLength + 1];
    strncpy (foo, tokenStart, tokenLength);
    foo[tokenLength] = 0;
    errStr ("Unable to understand token '%s'; ignoring.\n", foo);
    //return false;
  } // while getToken()

  numDefs ++;
  return true;
}


// Add a subfield to a field definiition
static bool addField (int start, int end, bool invert) {

  // Enough room?
  if (fields[numFields].numSubFields >= MAX_SUB_FIELDS) {
    err ("Too many subfields\n");
    return false;
  }

  // Add
  fields[numFields].subFields[fields[numFields].numSubFields].startBit = start;
  fields[numFields].subFields[fields[numFields].numSubFields].endBit = end;
  fields[numFields].subFields[fields[numFields].numSubFields].invert = invert;
  fields[numFields].numSubFields ++;
  fields[numFields].bitCnt += start - end + 1;
  return true;
}

// Read field name and subfields
static bool doFieldDef (bool subfield) {
  // Enough room?
  if (numFields >= MAX_FIELDS) {
    err ("Too many fields\n");
    return false;
  }

  // Get field name
  if (! getToken ()) {
    err ("Unable to parse field name; ignoring.\n");
    return false;
  }
  if (tokenLength > MAX_NAME_LEN) {
    err ("Field name too long; ignoring.\n");
    return false;
  }
  strncpy (fields[numFields].fieldName, tokenStart, tokenLength);
  fields[numFields].fieldName[tokenLength] = 0;
  fields[numFields].numSubFields = 0;
  fields[numFields].bitCnt = 0;
  fields[numFields].subfield = subfield;

  // Duplicate?
  if (dupCheck (-1)) {
    err ("Duplicate field name; ignoring.\n");
    return false;
  }

  // Get field start
  if (! getToken ()) {
    err ("Unable to parse field start; ignoring.\n");
    return false;
  }

  while (1) { // Accumulate fiields
    // Subfield inverted?
    bool invert = false;
    if (tokeneq ("~")) {
      // Consume tilde
      if (! getToken ()) {
        err ("Unable to parse field start; ignoring.\n");
        return false;
      }
      invert = true;
    }

    // Parse field start
    char * endPtr;
    unsigned long start = strtoul (tokenStart, & endPtr, 0);
    if (endPtr - tokenStart != tokenLength) {
      err ("Unable to parse field start; ignoring.\n");
      return false;
    }
    if (start >= ROM_WIDTH) {
      fprintf (fList, "Field start %lu too big; ignoring.\n", start);
      return false;
    }

    // Next token is
    //  colon   field is start : end
    //  number or no token:  field is start : start, number is the next start

    // Get next token
    if (! getToken ()) {
      // No more tokens
      if (! addField (start, start, invert)) {
        return false;
      }
      break;
    }

    // Colon?
    if (tokeneq (":")) {

      // Get field end
      if (! getToken ()) {
        err ("Unable to parse field end; ignoring.\n");
        return false;
      }
      char * endPtr;
      unsigned long end = strtoul (tokenStart, & endPtr, 0);
      if (endPtr - tokenStart != tokenLength) {
        err ("Unable to parse field end; ignoring.\n");
        return false;
      }
      if (end >= ROM_WIDTH) {
        err ("Field end too big; ignoring.\n");
        return false;
      }

      // Add field:field
      if (! addField (start, end, invert)) {
        return false;
      }

      // Consume field end
      if (! getToken ()) {
        break;
      }
    } else {
      // // not colon, check to see if it is a number;
      // continue;
      // Add field
      if (! addField (start, start, invert)) {
        return false;
      }
    }
  }

  numFields ++;

  return true;
}


static bool doGroup (void) {
  // Get field name
  if (! getToken ()) {
    err ("Unable to parse field name; ignoring.\n");
    return false;
  }
  int g = atoi (tokenStart);
  if (g < 0 || g >= NUM_GROUPS) {
    err ("group number must be 0-31; ignoring.\n");
    return false;
  }
  currentGroup = g;
  return true;
}


  
static bool doTag (void) {

  // Get field name
  if (! getToken ()) {
    err ("Unable to parse field name; ignoring.\n");
    return false;
  }

  // Lookup field name
  int fieldNumber;
  for (fieldNumber = 0; fieldNumber < numFields; fieldNumber ++)
    if (tokeneq (fields[fieldNumber].fieldName))
      break;
  if (fieldNumber >= numFields) {
    err ("Field name not found; ignoring.\n");
    return false;
  }

  // Parse tag name/value pairs
  while (getToken ()) {
    // Enough room?
    if (numTags >= MAX_TAGS) {
      err ("Too many tags; ignoring.\n");
      return false;
    }
    // Duplicate
    if (dupCheck (fieldNumber)) {
      err ("Duplicate tag name; ignoring.\n");
      return false;
    }
    char tagName[MAX_NAME_LEN];
    strncpy (tagName, tokenStart, tokenLength);
    tagName[tokenLength] = 0;

    // Parse value
    if (! getToken ()) {
      err ("Tag value not found; ignoring.\n");
      return false;
    }
    if (tokenLength >= MAX_NAME_LEN) {
      err ("Tag value too long; ignoring.\n");
      return false;
    }

    newBitstring (bits);
    int numBits;
    if (! parseNumberBits (& bits, & numBits)) {
      err ("Can't parse tag value\n");
      return false;
    }
    strcpy (tags[numTags].tagName, tagName);
    setBitsBits (& bits, & fields[fieldNumber], & tags[numTags].tagBits);
    tags[numTags].fieldNumber = fieldNumber;
    numTags ++;
  } // while getToken
  return true;
}


// Parse include

static bool doInclude (void) {

    // skip white space
    while (isspace (*tokenPtr))
      tokenPtr ++;
    tokenStart = tokenPtr;

    // Find trailing white space or eos
    while (! isspace (*tokenPtr))
      tokenPtr ++;
    tokenLength = tokenPtr - tokenStart;

    // Discard to EOS
    while (*tokenPtr)
      tokenPtr ++;

    char fname [tokenLength + 1];
    strncpy (fname, tokenStart, tokenLength);
    fname[tokenLength] = 0;

    FILE * fp  = fopen (fname, "r");
    if (! fp)  {
      errStrStr ("Error '%s' opening include file \"%s\", skipping", strerror (errno), fname);
      return false;
    }

    inputFileStackPtr ++;
    FTOP.inputFile = fp;
    FTOP.lineNumber = 0;
    FTOP.inputBuffer = NULL;
    FTOP.inputBufferLength = 0;

    return true;
  }


// Disassemble

// @
static bool doAddr (void) {
  uint64_t addr;

  // Parse address
  if (! getToken ()) {
    err ("Unable to parse address operand; ignoring.\n");
    return true;
  }

  // Must be a number
  char * endPtr;
  addr = strtoul (tokenStart, & endPtr, 0);
  if (endPtr - tokenStart != tokenLength) {
    err ("Unable to parse address operand; ignoring.\n");
    return true;
  }

  if (addr >= ROM_LENGTH) {
    errLong ("Address %lu too big; ignoring.\n", addr);
    return true;
  }

  romAddress = addr;
  memset (& ROMWords[romAddress], 0, sizeof (ROMWords[romAddress]));
  return true;
}


static char disAsmBuffer[NUM_GROUPS][65536];



static void doDefRef (struct def * defPtr) {
  uint16_t value = 2048;
  // Does the def have a parameter?
  if (defPtr->paramFieldIdx >= 0) {
    // Get parameter value
    if (! getToken ()) {
      err ("Unable to parse parameter value; ignoring.\n");
      return;
    }
    // Is it a label?
    int labelNumber = numLabels;
    if (defPtr->defType == defLabel || defPtr->defType == defLabelShort || defPtr->defType == defLabelHigh) {
      for (labelNumber = 0; labelNumber < numLabels; labelNumber ++) {
        if (tokeneq (labels[labelNumber].labelName)) {
          break;
        }
      }
    }
    // found?
    if (labelNumber < numLabels) {
      value = labels[labelNumber].value;
      enum defTypes defType = defPtr->defType;
      if (defType == defLabelShort && (value & SHORT_LABEL_MASK_HIGH) != (romAddress & SHORT_LABEL_MASK_HIGH)) {
        err ("label is too far away\n");
      }
      newBitstring (bits);
      setBitsFieldBitsValue (value, fields + defPtr->paramFieldIdx, & bits);
      updateBits (& ROMWords[romAddress], & bits);
    } else { // Number
      newBitstring (valueBits);
      int numBits;
      if (parseNumberBits (& valueBits, & numBits)) {
        value = toInt (& valueBits);
        newBitstring (bits);
        setBitsBits (& valueBits, fields + defPtr->paramFieldIdx, & bits);
        //updateBits (& defPtr->bits, & bits);
        updateBits (& ROMWords[romAddress], & bits);
      } else {
        err ("Can't grok parameter value\n");
      }
    }
  }

  updateBits (& ROMWords[romAddress], & defPtr->defBits);
  // Is the parameter a target label?
  if (defPtr->targetAnnotationP) {
    // Enough room?
    if (targetAnnotations[romAddress].numTargets >= MAX_TARGET_ANNOTATIONS) {
      err ("Too many targets\n");
    } else {
      targetAnnotations[romAddress].targets[targetAnnotations[romAddress].numTargets] = value;
      targetAnnotations[romAddress].numTargets ++;
    }
  }
  // Is the parameter a target++?
  if (defPtr->targetppAnnotationP) {
    // Enough room?
    if (targetAnnotations[romAddress].numTargets >= MAX_TARGET_ANNOTATIONS) {
      err ("Too many targets\n");
    } else {
      targetAnnotations[romAddress].targets[targetAnnotations[romAddress].numTargets] = romAddress + 1;
      targetAnnotations[romAddress].numTargets ++;
    }
  }
}


static bool doFieldRef (struct field * fldPtr) {

  struct field field = * fldPtr;

  // <=
  if (! getToken ()) {
    err ("Unable to parse field reference operaror; ignoring.\n");
    return false;
  }
  if (! tokeneq ("<=")) {
    err ("Unable to parse field reference operaror; ignoring.\n");
    return false;
  }

  // value
  if (! getToken ()) {
    err ("Unable to parse field value; ignoring.\n");
    return false;
  }
  int numBits;
  newBitstring (bits);

  // Is value a tag?
  int tagNumber;
  for (tagNumber = 0; tagNumber < numTags; tagNumber ++)
    //if (tokeneq (tags[tagNumber].tagName) && fieldNumber == tags[tagNumber].fieldNumber)
    if (tokeneq (tags[tagNumber].tagName) && fldPtr == fields + tags[tagNumber].fieldNumber)
      break;
  // found?
  if (tagNumber < numTags) {
    bits = tags[tagNumber].tagBits;
    // set the ROM bits
    setROMBits (& bits);
    return true;
  } 

  // Is value a number?
  if (parseNumberBits (& bits, & numBits)) {
    // Map bits
    newBitstring (mapped);
    setBitsBits (& bits, & field, & mapped);
    setROMBits (& mapped);
    return true;
  }

  char foo [tokenLength + 1];
  strncpy (foo, tokenStart, tokenLength);
  foo[tokenLength] = 0;
  errStr ("Unable to understand field value '%s'; ignoring.\n", foo);
  return false;
}

static char ubuf[100000000];
static char tmp[100000000];

#define eos(p) while (*(p)) (p) ++
static inline char * strcat_ (int group, char * p, char * s) {
  if (strlen (disAsmBuffer[group]) + strlen (s) + 1 > sizeof (disAsmBuffer[group])) {
    fprintf (stderr, "disAsmBuffer too small\n");
    return p;
  }
  strcat (p, s);
  eos (p);
  return p;
}

static void rewriteSource (char * srcFname, bool append) {

  // Copy source file to .bak
  char cmd[1024];
  sprintf (cmd, "cp %s %s.bak", srcFname, srcFname);
  int rc = system (cmd);
  if (rc)
    fprintf (stderr, "system (cp) returned %d\n", rc);


  // Open source file
  int ufd = open (srcFname, O_RDWR);
  if (ufd < 0) {
    perror ("rewriteSource open");
    exit (1);
  }

  // Size of source file
  off_t ufdSize = lseek (ufd, 0L, SEEK_END);
  lseek (ufd, 0L, SEEK_SET);
  if (ufdSize + 1 > sizeof (ubuf)) {
    fprintf (stderr, "ubuf too small\n");
    return;
  }

  size_t usz = read (ufd, ubuf, sizeof (ubuf));
  //fprintf (stderr, "CodeROM.uasm %ld\n", usz);
  ubuf[usz] = 0;

  //
  // Disassemble
  //

  // For each ROM word
  for (int addr = 0; addr < ROM_LENGTH; addr ++) {
    uint64_t word = CodeROM[addr];

    // Disassemble into disAsmBuffer
    char * bp[NUM_GROUPS];
    for (int i = 0; i < NUM_GROUPS; i ++) {
      bp[i] = disAsmBuffer[i];
      *(bp[i]) = 0;
    }


    // Generate "@addr" line
    bp[0] += sprintf (bp[0], "@0x%03x", addr);

    // Add labels
    for (int labelNum = 0; labelNum < numLabels; labelNum ++) {
      if (labels[labelNum].value == addr) {
        bp[0] += sprintf (bp[0], ":%s", labels[labelNum].labelName);
      }
    }

    // New line
    bp[0] += sprintf (bp[0], "\n");


    //
    // Search defs looking for matches in the ROM word;
    //

    // Create a struct bitstring from the ROM word
    // Mark the bits as set, and unset them as definitions are found
    struct bitstring ROMBits;
    for (int bitNum = 0; bitNum < ROM_WIDTH; bitNum ++) {
      ROMBits.bits[bitNum] = getBit (word, bitNum);
      ROMBits.set[bitNum] = true;
    }


    // The defs are searched in reverse order, allowing more complex defs to be built from simpler ones
    for (int defNum = numDefs - 1; defNum >= 0; defNum --) {
      struct def * dp = defs + defNum;
      if (! bitsBitsMatch (& ROMBits, dp))
        continue;

      // Match found; add this def name to the def line and mark the ROM bits as unset
      if (! dp->default_) {
        bp[dp->group] = strcat_ (dp->group, bp[dp->group], dp->defName);
      }

      // Does the def have a parameter?
      if (dp->paramFieldIdx >= 0) {
        // The field to be set to the parameter value
        struct field * fp = fields + dp->paramFieldIdx;
        // XXX do label lookup
        char paramValueString[ROM_WIDTH + 1];
        char paramValueStringx[ROM_WIDTH + 1];
        char paramValueStringHex[ROM_WIDTH + 1];
        uint64_t paramValue;
        newBitstring (matchedBits);
        bitsBitsPartialMatch (& ROMBits, fp, & matchedBits, paramValueString, paramValueStringx, & paramValue);
        int labelNumber = numLabels;
        //if (dp->defType == defLabel || dp->defType == defLabelShort) 
        if (dp->defType == defLabel) {
          for (labelNumber = 0; labelNumber < numLabels; labelNumber ++)
            if (labels[labelNumber].value == paramValue )
              break;
        } else if (dp->defType == defLabelShort) {
          uint64_t target = (addr & SHORT_LABEL_MASK_HIGH) | (paramValue & SHORT_LABEL_MASK_LOW);
          for (labelNumber = 0; labelNumber < numLabels; labelNumber ++)
            if (labels[labelNumber].value == target)
              break;
        } else if (dp->defType == defLabelHigh) {
          uint64_t target = paramValue << 4;
          for (labelNumber = 0; labelNumber < numLabels; labelNumber ++)
            if (labels[labelNumber].value == target)
              break;
        }
        if (labelNumber < numLabels) {
          bp[dp->group] = strcat_ (dp->group, bp[dp->group], " ");
          bp[dp->group] = strcat_ (dp->group, bp[dp->group], labels[labelNumber].labelName);
        } else {
          if (dp->defType == defLabel) {
            bp[dp->group] = strcat_ (dp->group, bp[dp->group], " 0x");
            bp[dp->group] = strcat_ (dp->group, bp[dp->group], binToHex (paramValueStringHex, paramValueString));
          } else {
            bp[dp->group] = strcat_ (dp->group, bp[dp->group], " 0b");
            bp[dp->group] = strcat_ (dp->group, bp[dp->group], paramValueString);
          }
        }
        for (int bitNum = 0; bitNum < ROM_WIDTH; bitNum ++) {
          if (matchedBits.set[bitNum]) {
            if ((! ROMBits.set[bitNum]) && (ROMBits.bits[bitNum] != matchedBits.bits[bitNum])) {
               errInt ("updateBits: bit %d already programmed\n", bitNum);
            }
            ROMBits.bits[bitNum] = matchedBits.bits[bitNum];
            ROMBits.set[bitNum] = false;
          }
        }
      }  // Does the def have a parameter?



      if (! dp->default_) {
        char * p = strchr (defs[defNum].defBody, ';');
        if (p) {
          bp[dp->group] = strcat_ (dp->group, bp[dp->group], " ");
          bp[dp->group] = strcat_ (dp->group, bp[dp->group], p);
        }
        bp[dp->group] = strcat_ (dp->group, bp[dp->group], "\n");
      }

      for (int bitNum = 0; bitNum < ROM_WIDTH; bitNum ++) {
        if (dp->defBits.set[bitNum])
          ROMBits.set[bitNum] = false;
      }
//#define TRACE_ROM
#ifdef TRACE_ROM
char val[ROM_WIDTH + 1];
bitstringToString (& ROMBits, /* *numBits */ 56, val);
//fprintf (stderr, "b:      <%s> <%s>\n", foo, val);
bp[dp->group] = strcat_ (dp->group, bp[dp->group], "; in defNum loop ");
bp[dp->group] = strcat_ (dp->group, bp[dp->group], val);
bp[dp->group] = strcat_ (dp->group, bp[dp->group], "\n");
#endif
    } // for defNum

#ifdef TRACE_ROM
char val[ROM_WIDTH + 1];
bitstringToString (& ROMBits, /* *numBits */ 56, val);
//fprintf (stderr, "b:      <%s> <%s>\n", foo, val);
bp[NUM_GROUPS - 1] = strcat_ (NUM_GROUPS - 1, bp[NUM_GROUPS - 1], "; after defNum loop ");
bp[NUM_GROUPS - 1] = strcat_ (NUM_GROUPS - 1, bp[NUM_GROUPS - 1], val);
bp[NUM_GROUPS - 1] = strcat_ (NUM_GROUPS - 1, bp[NUM_GROUPS - 1], "\n");
#endif

    // Walk the remaining ROM word bits looking for fields with bits still set
    int nhits = 0;
    for (int fieldNum = 0; fieldNum < numFields; fieldNum ++) {
      struct field * fp = fields + fieldNum;
      if (fp->subfield)
        continue;
      char number[ROM_WIDTH + 1];
      char numberx[ROM_WIDTH + 1];
      newBitstring (matchedBits)
      uint64_t value;
      if (! bitsBitsPartialMatch (& ROMBits, fp, & matchedBits, number, numberx, & value))
        continue;
      // Generate field ref  
      if (nhits) {
        bp[NUM_GROUPS - 1] = strcat_ (NUM_GROUPS - 1, bp[NUM_GROUPS - 1], " ");
      }
      bp[NUM_GROUPS - 1] = strcat_ (NUM_GROUPS - 1, bp[NUM_GROUPS - 1], " ");
      bp[NUM_GROUPS - 1] = strcat_ (NUM_GROUPS - 1, bp[NUM_GROUPS - 1], fp->fieldName);
      bp[NUM_GROUPS - 1] = strcat_ (NUM_GROUPS - 1, bp[NUM_GROUPS - 1], "<=0b");
      bp[NUM_GROUPS - 1] = strcat_ (NUM_GROUPS - 1, bp[NUM_GROUPS - 1], numberx);
      bp[NUM_GROUPS - 1] = strcat_ (NUM_GROUPS - 1, bp[NUM_GROUPS - 1], "\n");
    }
    bp[NUM_GROUPS - 1] = strcat_ (NUM_GROUPS - 1, bp[NUM_GROUPS - 1], " !");


    // Scan source lookng for matching @addr
    char atAddr[7];
    sprintf (atAddr, "@0x%03x", addr);
    char * atup;
    atup = strstr (ubuf, atAddr);
    if (atup) { // found
      char * bangup;
      bangup =  strchr (atup, '!');
      if (! bangup) {
        fprintf (stderr, "no uasm bang after %ld\n", atup - ubuf);
        break; // while atdp
      }
      // Copy everythng after the uasm bang to tmp
      strcpy (tmp, bangup + 1);
      // Nuke at the @
      * atup = 0;
      // Append the dis string
      //strcpy (atup, disAsmBuffer);
      for (int i = 0; i < NUM_GROUPS; i ++)
        strcat (atup, disAsmBuffer[i]);
      // Append tmp back on
      strcat (atup, tmp);
    } else if (append) { // not found, append to the end

// XXX 
//  \usepackage{verbatim}
//  \begin{comment}
// ...
//  \end{comment}
//
//  \iffalse
//  \fi


      for (int i = 0; i < NUM_GROUPS; i ++) {
// 2 is the trailing null plus the newline added later
        if (strlen (ubuf) + strlen (disAsmBuffer[i]) + 2 > sizeof (ubuf)) {
          fprintf (stderr, "ubuf too small\n");
          exit (2);
        }
        strcat (ubuf, disAsmBuffer[i]);
      }
      strcat (ubuf, "\n");
    }
  } // for addr
  fprintf (stderr, "ubuf usage %ld%%\n", strlen (ubuf) * 100 / sizeof (ubuf));
  lseek (ufd, SEEK_SET, 0);
  write (ufd, ubuf, strlen (ubuf));
  ftruncate (ufd, strlen (ubuf));
  close (ufd);

}



static void parseInputBuffer (void) {
  tokenPtr = FTOP.inputBuffer;
  while (getToken ()) {

    // eof
    if (tokeneq ("begin")) {
      long offset = ftell (FTOP.inputFile);
      fclose (FTOP.inputFile);
      rewriteSource (srcFname, true);
      rewriteSource (docFname, true);
      FTOP.inputFile = fopen (srcFname, "r");
      if (! FTOP.inputFile) {
        perror ("Opening source file");
        exit (1);
      }
      fseek (FTOP.inputFile, offset, SEEK_SET);
      continue;
    }

    // eof
    if (tokeneq ("eof")) {
      fseek (FTOP.inputFile, 0, SEEK_END);
      continue;
    }

    // @
    if (tokeneq ("@")) {
      if (! doAddr ())
        break;
      continue;
    }

    // include
    if (tokeneq ("include")) {
      if (! doInclude ())
        break;
      continue;
    }

    // field
    if (tokeneq ("field")) {
      if (! doFieldDef (false))
        break;
      continue;
    }

    // subfield
    if (tokeneq ("subfield")) {
      if (! doFieldDef (true))
        break;
      continue;
    }

    // tag
    if (tokeneq ("tag")) {
      if (! doTag ())
        break;
      continue;
    }

    // group
    if (tokeneq ("group")) {
      if (! doGroup ())
        break;
      continue;
    }

    // def
    if (tokeneq ("def")) {
      // doDefDef always returns false so that the rest of the line (containing the macro body) will be skipped
      if (! doDefDef (defNormal))
        break;
      continue;
    }

    // defd
    if (tokeneq ("defd")) {
      // doDefDef always returns false so that the rest of the line (containing the macro body) will be skipped
      if (! doDefDef (defDefault))
        break;
      continue;
    }

    // def1
    if (tokeneq ("def1")) {
      // doDefDef always returns false so that the rest of the line (containing the macro body) will be skipped
      if (! doDefDef (defOneParameter))
        break;
      continue;
    }

    // defl
    if (tokeneq ("defl")) {
      // doDefDef always returns false so that the rest of the line (containing the macro body) will be skipped
      if (! doDefDef (defLabel))
        break;
      continue;
    }

    // defls
    if (tokeneq ("defls")) {
      // doDefDef always returns false so that the rest of the line (containing the macro body) will be skipped
      if (! doDefDef (defLabelShort))
        break;
      continue;
    }

    // deflh
    if (tokeneq ("deflh")) {
      // doDefDef always returns false so that the rest of the line (containing the macro body) will be skipped
      if (! doDefDef (defLabelHigh))
        break;
      continue;
    }

    if (tokeneq ("label")) {
      if (! doLabelDef (false))
        break;
      continue;
    }

    if (tokeneq ("labelu")) {
      if (! doLabelDef (true))
        break;
      continue;
    }

    if (tokeneq ("oplabel")) {
      if (! doOpLabelDef ())
        break;
      continue;
    }

    if (tokeneq ("target")) {
      if (! doTarget ())
        break;
      continue;
    }

    if (tokeneq ("usage")) {
      if (! doUsage ())
        break;
      continue;
    }

    // !
    if (tokeneq ("!")) {
      if (! doWrite ())
        break;
      continue;
    }

    // :
    if (tokeneq (":")) {
      if (! doLabel ())
        break;
      continue;
    }

    // field name?
    int fieldNumber;
    for (fieldNumber = 0; fieldNumber < numFields; fieldNumber ++) {
      if (tokeneq (fields[fieldNumber].fieldName)) {
        break;
      }
    }
    // found?
    if (fieldNumber < numFields) {
      if (! doFieldRef (fields + fieldNumber))
        break;
      continue;
    }

    // def name?
    int defNumber;
    for (defNumber = 0; defNumber < numDefs; defNumber ++) {
      if (tokeneq (defs[defNumber].defName)) {
        break;
      }
    }
    // found?
    if (defNumber < numDefs) {
      doDefRef (defs + defNumber);
      continue;
    }


    // Give up
    char foo [tokenLength + 1];
    strncpy (foo, tokenStart, tokenLength);
    foo[tokenLength] = 0;
    errStr ("Huh? '%s'; ignoring.\n", foo);
  } // while getToken
} // parseInpoutBuffer


static void pass () {
  romAddress = 0;
  FTOP.lineNumber = 0;
  while (1) { // unnest includes
    while (! feof (FTOP.inputFile)) {
      int rc = getline (& FTOP.inputBuffer, & FTOP.inputBufferLength, FTOP.inputFile);
      if (rc == -1)
        break;
      FTOP.lineNumber ++;
      fprintf (fList, "    %5d   %s", FTOP.lineNumber, FTOP.inputBuffer);
      parseInputBuffer ();
    }
//    if (inputFileStackPtr == 1)
//      break;
    fclose (FTOP.inputFile);
    inputFileStackPtr --;
    if (inputFileStackPtr < 0)
      break;
  }
}

static void usage (void) {
  fprintf (stderr, "Usage:\n"
                   "  uasm name\n"
                   "  Assembles name.uasm to name.rom, listing to name.lst\n");
  exit (1);
}


static void listSymbols (void) {

  ///
  /// Fields
  ///

  fprintf (fList, "\n\n Fields\n");
  for (int fieldNumber = 0; fieldNumber < numFields; fieldNumber ++) {
    fprintf (fList, "%3d %s %s [%d]", fieldNumber, fields[fieldNumber].subfield ? "[sub]" : "", fields[fieldNumber].fieldName, fields[fieldNumber].bitCnt);
    int nBits = 0;
    for (int j = 0; j < fields[fieldNumber].numSubFields; j ++) {
      struct subField * p = fields[fieldNumber].subFields + j;
      char * tilde = p->invert ? "~" : "";
      if (p->startBit == p->endBit) {
        fprintf (fList, " %s%d", tilde, p->startBit);
      } else {
        fprintf (fList, "  %s%d:%d", tilde, p->startBit, p->endBit);
      }
      nBits += p->startBit - p->endBit + 1;
    }
    fprintf (fList, "\n");
    bool hdr = false;
    for (int t = 0; t < numTags; t ++) {
      if (tags[t].fieldNumber != fieldNumber)
        continue;
      if (! hdr) {
        hdr = true;
        fprintf (fList, "    Tags\n");
      }
      char val[ROM_WIDTH + 1];
      bitstringToString (& tags[t].tagBits, fields[fieldNumber].bitCnt, val);
      fprintf (fList, "      %s %s\n", tags[t].tagName, val + (56 - nBits));
    }
  }

  ///
  /// Labels
  ///
  fprintf (fList, "\n\n Labels\n");
  for (int i = 0; i < numLabels; i ++) {
    fprintf (fList, " %03lx %s\n", labels[i].value, labels[i].labelName);
  }

  ///
  /// Defs
  ///
  fprintf (fList, "\n\n Defs\n");
  for (int i = 0; i < numDefs; i ++) {
    fprintf (fList, "%3d %s %d %d: %s\n", i, defs[i].defName, defs[i].group, defs[i].paramFieldIdx, defs[i].defBody);
    char buffer[ROM_WIDTH + 1];
    bitstringToString (& defs[i].defBits, ROM_WIDTH, buffer);
    fprintf (fList, "  %s\n"
                    "  55555544444444443333333333222222222211111111110000000000\n"
                    "  54321098765432109876543210987654321098765432109876543210\n",
                    buffer);
  }

  ///
  /// ROM fields
  ///
  fprintf (fList, "\n\n ROM fields\n");
  for (int addr = 0; addr < ROM_LENGTH; addr ++) {
    uint64_t word = CodeROM[addr];
    fprintf (fList, " %03x %014lx ", addr, word);
    struct bitstring foo;
    for (int bitNum = 0; bitNum < ROM_WIDTH; bitNum ++) {
      foo.bits[bitNum] = getBit (word, bitNum);
      foo.set[bitNum] = true;
    }
    for (int fieldNum = numFields - 1; fieldNum >= 0; fieldNum --) {
      struct field * fp = fields + fieldNum;
      if (fp->subfield)
        continue;
      char number[ROM_WIDTH + 1];
      char numberx[ROM_WIDTH + 1];
      newBitstring (matchedBits)
      uint64_t value;
      bitsBitsPartialMatch (& foo, fp, & matchedBits, number, numberx, & value);
      fprintf (fList, " %s<=0b%s", fp->fieldName, number);
    }
    fprintf (fList, "\n");
  }

  ///
  /// Targets
  ///

  fprintf (fList, "\n\n Targets\n");
  for (int address = 0; address < ROM_LENGTH; address ++) {
    if (targetAnnotations[address].numTargets) {
      int labelNumber;
      for (labelNumber = 0; labelNumber < numLabels; labelNumber ++) {
        if (labels[labelNumber].value == address)
          break;
      }
      fprintf (fList, "  0x%03x (%s)", address, labelNumber < numLabels ? labels[labelNumber].labelName : "");
      for (int i = 0; i < targetAnnotations[address].numTargets; i ++) {
        for (labelNumber = 0; labelNumber < numLabels; labelNumber ++) {
          if (labels[labelNumber].value == targetAnnotations[address].targets[i])
            break;
        }
        fprintf (fList, "  0x%03lx (%s)", targetAnnotations[address].targets[i], labelNumber < numLabels ? labels[labelNumber].labelName : "");
      }
      fprintf (fList, "\n");
    }
  }

  ///
  /// usageMap
  ///
  fprintf (fList, "\n\n Usage map\n");
  for (int i = 0; i < ROM_LENGTH; i ++) {
    fprintf (fList, "  0x%03x %s\n", i, usageMap[i]);
  }
}


// starting at microcode address 0, walk the targerAnnotaions table to visit every instruction that
// is executed from there, propagating the usage map along the way

static bool visited[ROM_LENGTH];

static void pum (uint64_t addr) {
  if (addr >= ROM_LENGTH)
    return;

  if (visited[addr])
    return;
  visited[addr] = true;

  for (int i = 0; i < targetAnnotations[addr].numTargets; i ++) {
    uint64_t ta = targetAnnotations[addr].targets[i];
    if (ta >= ROM_LENGTH)
      continue;
    fprintf (fList, "    0x%03lx (%s) => 0x%03lx (%s)\n", addr, usageMap[addr], ta, usageMap[ta]);
    if (! usageMap[ta][0]) {
      strcpy (usageMap[ta], usageMap[addr]);
    }
    pum (ta);
  }
}

static void populateUsageMap (void) {
  fprintf (fList, "\n\n Usage map tree\n");
  memset (& visited, 0, sizeof (visited));
  pum (0x000);
}

int main (int argc, char * argv []) {
  if (argc != 2)
    usage ();

  memset (& usageMap, 0, sizeof (usageMap));
  memset (& targetAnnotations, 0, sizeof (targetAnnotations));

  // Open source file
  srcFname = malloc (strlen (argv[1]) + strlen (IN_EXT) + 1);
  strcpy (srcFname, argv[1]);
  strcat (srcFname, IN_EXT);
  inputFileStackPtr = 0;
  FTOP.inputFile = fopen (srcFname, "r");
  if (! FTOP.inputFile) {
    perror ("Opening source file");
    exit (1);
  }
  FTOP.lineNumber = 0;
  FTOP.inputBuffer = NULL;
  FTOP.inputBufferLength = 0;

  // Docfile name
  docFname = malloc (strlen (argv[1]) + strlen (DOC_EXT) + 1);
  strcpy (docFname, argv[1]);
  strcat (docFname, DOC_EXT);

#ifdef WRITE_ROM
  char romFname [strlen (argv[1]) + strlen (ROM_EXT) + 1];
  strcpy (romFname, argv[1]);
  strcat (romFname, ROM_EXT);
  fROM = fopen (romFname, "w");
  if (! fROM) {
    perror ("Opening ROM output file");
    exit (1);
  }
#endif

  char lstFname [strlen (argv[1]) + strlen (LST_EXT) + 1];
  strcpy (lstFname, argv[1]);
  strcat (lstFname, LST_EXT);
  fList = fopen (lstFname, "w");
  if (! fList) {
    perror ("Opening listing file");
    exit (1);
  }

  pass ();
#ifdef WRITE_ROM
  for (int i = 0; i < ROM_LENGTH; i ++) {
    uint64_t word = toInt (& ROMWords[i]);
    fprintf (fROM, "%014lx\n", word);
  }
#endif
  populateUsageMap ();
  listSymbols ();
} 


