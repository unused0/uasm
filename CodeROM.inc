field INT_LVL 55
field RLO 53
field SFT_CRY 52:51
field ARA_SEL 50:47
field ARB_SEL 46:43
field ALU_DST 42:40
field ALU     39:34
field CND_ENB 33
field SEQ_CTL 54 32:29
field STK_CTL 28:27
field SEQ_DATA 26:16
field CND_JSR 15
field CTL5 14:13
field CTL4 12:10
field REG_LD3 9:7
field REG_LD2 6:4
field BUS_SEL 3:0

;label INIT0 0x000
;label INIT1 0x0b7

; https://github.com/sjsoftware/centurion-cpu6/wiki/Microcode
; Centurion/Verilog.CPU6.v
; Schematics from https://github.com/sjsoftware/centurion-cpu6.git



;;;
;;; 3:0 BUS_SEL
;;;

; Bit PROM      Label    
;  3  UE3 D3    MC3     UD3 E3
;  2  UE3 D2    MC2     UD3 A2
;  1  UE3 D1    MC1     UD3 A1
;  0  UE3 D0    MC0     UD3 A0



; /READ.SWP Set BUS_SEL to 0000
;    Sets UD3 O0 to 0; latched on UD5 Q0 as ~READ.SWP 
;      LOAD.SWP latches DP7:DP0; 
;         LOAD.SWP is PL10 & PL11 & PL12
;           PL12:PL10 are latched MC12:MC10 (CTL4)
;      /READ.SWP puts the latched values on DP7:DP0 nibble-swapped.

def /READ.SWP BUS_SEL<=0b0000  ; swap register
def /READ.RF  BUS_SEL<=0b0001  ; reg_ram_data_out
def /READ.AHI BUS_SEL<=0b0010  ; { ~memory_address[15:12], memory_address[11:8] }
def /READ.ALO BUS_SEL<=0b0011  ; memory_address[7:0];
def XXX_BUS_SEL_0100 BUS_SEL<=0b0100
def XXX_BUS_SEL_0101 BUS_SEL<=0b0101
def XXX_BUS_SEL_0110 BUS_SEL<=0b0110
def XXX_BUS_SEL_0111 BUS_SEL<=0b0111
def /READ.PF  BUS_SEL<=0b1000  ; current page file entry
def /READ.CCR BUS_SEL<=0b1001  ; condition code register/sense switches
def /READ.DB  BUS_SEL<=0b1010  ; data bus from the AM2907 receive latches
def /READ.MSR BUS_SEL<=0b1011  ; machine status register/current interrupt level
def /READ.INR BUS_SEL<=0b1100  ; interrupt request level/dip switches
def /READ.CON BUS_SEL<=0b1101  ; constant (inverted microcode bits)
def XXX_BUS_SEL_1110 BUS_SEL<=0b1110
def XXX_BUS_SEL_1111 BUS_SEL<=0b1111


;;;
;;; 6:4 REG_LD2
;;;

; Bit PROM      Label   Latch        Demultiplexer
;  6  UE3 D6    MC6     UE5 Q4  PL6  UE6 A2
;  5  UE3 D5    MC5     UE5 Q0  PL5  UE6 A1
;  4  UE3 D4    MC4     UE5 Q3  PL4  UE6 A0

def XXX_REG_LD2_000 REG_LD2<=0b000
def /LOAD.RR  REG_LD2<=0b001   ; load result register, used to store ALU results
def /LOAD.RIR REG_LD2<=0b010   ; load register index register
def /LOAD.ILR REG_LD2<=0b011   ; load interrupt level register (this might get renamed)
;
; LOAD.MAP Latches F2 F1 F0 to MAP2 MAP1 MAP0
;
def /LOAD.MAP REG_LD2<=0b100   ; load map register
def /LOAD.MAR REG_LD2<=0b101   ; load machine address register (PC)
def /LOAD.SAR REG_LD2<=0b110   ; load sequencer address register
def /LOAD.CCR REG_LD2<=0b111   ; load condition code register (this might get renamed)



;;;
;;; 9:7 REG_LD3
;;;

; Bit PROM     Label Latch
;  9  UJ3 D1   MC9   UJ5 Q6  PL9   UK11 A2
;  8  UJ3 D0   MC8   UJ5 Q7  PL8   UK11 A1
;  7  UE3 D7   MC7   UE5 Q5  PL7   UK11 A0     ~WRITE.RF



def XXX_REG_LD3_000 REG_LD3<=0b000
def DMA.RESET REG_LD3<=0b001  ; ~K11.01
def CPU.CTL1  REG_LD3<=0b010  ; ~STATE2.EN B register select
                              ;     0/1: M13.0-/+       - unknown DMA control
                              ;     2/3: M13.1-/+       - unknown DMA control
                              ;     4/5: TIMER-/+       - timer enable/disable
                              ;     6/7: PROM-/+        - instruction decode PROM disable (not used?)
                              ;     8/9: RUN-/+         - run/halt light control
                              ;    10/11: TIMER.RES-/+ - timer reset
                              ;    12/13: ABORT-/+     - abort light control
                              ;    14/15: IACK-/+      - interrupt acknowledge
def CPU.CTL2  REG_LD3<=0b011  ; ~STATE1.EN B regisiter select
                              ;     0/1: INT-/+       - interrupt enable/disable
                              ;     2/3: ABE+/-       - address bus enable/disable for high bits (not used?)
                              ;     4/5: INC.DMA+/-   - set DMA address and count to increment
                              ;     6/7: DIR-/-       - increment direction on address registers
                              ;     8/9: DMA+/-       - set DMA active (DMA ACK? might be renamed)
                              ;   10/11: PARO-/+      - set even/odd parity
                              ;   12/13: PE-/+        - enable/disable parity error
                              ;   14/15: DMA.RD-/+    - DMA Ready (might be renamed)
def XXX_REG_LD3_100 REG_LD3<=0b100
def WRITE.PF  REG_LD3<=0b101  ; write page file
def LOAD.ALO  REG_LD3<=0b110  ; load address low byte
def LOAD.DBR  REG_LD3<=0b111  ; load AM2907 transmit registers


;;;
;;; 12:10 CTL4
;;;

; Bit PROM     Label Latch
; 12  UJ3 D4   MC12  UJ5 Q4  PL12  UH11 A2
; 11  UJ3 D3   MC11  UJ5 Q3  PL11  UH11 A1
; 10  UJ3 D2   MC10  UJ5 Q2  PL10  UH11 A0  
def /BUS.RD   CTL4<=0b001  ; Initiate a bus read cycle
def /BUS.WT   CTL4<=0b010  ; Initiate a bus write cycle
def /LOAD.AHI CTL4<=0b011  ; load the high byte of the address register
def /INC.WAR  CTL4<=0b100  ; increment the work address register
def /INC.MAR  CTL4<=0b101  ; increment the machine address register (PC)
def PROME    CTL4<=0b110  ; ALU/~PROM enable the PROM for initial instruction decoding
def /LOAD.SWP CTL4<=0b111  ; load the swap register


;;;
;;; 14:13 CTL5
;;;

; Bit PROM     Label Latch
; 14  UJ3 D6   MC14  UJ5 Q6  PL14  UE7 A1
; 13  UJ3 D5   MC13  UJ5 Q5  PL13  UE7 A0

def XXX_CTL5_000 CTL5<=0b00
def /BUS.ABT  CTL5<=0b01 ; 
def /LOAD.FLR CTL5<=0b10 ; 
def /BUS.WAIT CTL5<=0b11 ; 



;;;
;;; 15 CND_JSR
;;;

; Bit PROM     Label Latch
; 15  UJ3 D7   MC15  UJ5 Q0  PL15  UK9 ~E

; If Bit 15 is 0, /JSR? is selected from
; 1: JSR - used with bits 2-0 of Constant field (18:16) for conditional subroutine branch (microcode interrupt)
; 0: JSR BUSY - DMA operation busy (/BUSY)
; 1: JSR EVEN - both nibbles of register index are zero (RIR.0 NOR RIR.4)
; 2: JSR ODD  - low bit of register index is set (RIR.0)
; 3: JSR BADR - bad read address (NOT.MEM)
; 4: JSR BADW - bad write address  (B12.11)
; 5: JSR DMA  - DMA request (NOT DMA13.Q)
; 6: JSR PE   - Parity error (PE)
; 7: JSR INT  - Interrupt (INT)

def NO_JSR CND_JSR<=0b1  ; 15 is ~E
def JSR./BUSY CND_JSR<=0b0 SEQ_DATA<=0bxxxxxxxx000  ; Select ~BUSY
def JSR.EVEN CND_JSR<=0b0 SEQ_DATA<=0bxxxxxxxx001  ; Select RIR.0 XOR RIR.4
def JSR.ODD  CND_JSR<=0b0 SEQ_DATA<=0bxxxxxxxx010  ; Select RIR.0
def JSR./BADR CND_JSR<=0b0 SEQ_DATA<=0bxxxxxxxx011  ; Select NOT.MEMY
def JSR.BADW CND_JSR<=0b0 SEQ_DATA<=0bxxxxxxxx100  ; Select B12.11
def JSR./DMA  CND_JSR<=0b0 SEQ_DATA<=0bxxxxxxxx101  ; Select ~(DMA13.Q)
def JSR.PE   CND_JSR<=0b0 SEQ_DATA<=0bxxxxxxxx110  ; Select PE
def JSR.INT  CND_JSR<=0b0 SEQ_DATA<=0bxxxxxxxx111  ; Select INT



;;;
;;; 26:16 SEQ_DATA
;;;

; Bit PROM     Label Latch
; 26  UL3 D2   MC26  UL5 Q2  PL26 AM2911 UL9 D2
; 25  UL3 D1   MC25  UL5 Q6  PL25 AM2911 UL9 D1
; 24  UL3 D0   MC24  UL5 Q7  PL24 AM2911 UL9 D0            UJ10 S2
; 24  UM5 D7   MC23  UM5 Q0  PL23 AM2909 UJ7 D3  UK13 S1   UJ10 S1  UM6 I3a
; 22  UM5 D6   MC22  UM5 Q1  PL22 AM2909 UJ7 D2  UK13 S0   UJ10 S0  UM6 I2a
; 21  UM5 D5   MC21  UM5 Q5  PL21 AM2909 UJ7 D1  UJ13 S1   UJ10 ~E
; 20  UM5 D4   MC20  UM5 Q4  PL20 AM2909 UJ7 D0  UJ13 S0   UJ11 S1  
; 19  UM5 D3   MC19  UM5 Q3  PL19 AM2909 UK7 D3            UJ11 S0  UM6 I0b
; 18  UM5 D2   MC18  UM5 Q2  PL18 AM2909 UK7 D2  UK9 S2    UJ11 ~E  UM6 I1b 
; 17  UM5 D1   MC17  UM5 Q6  PL17 AM2909 UK7 D1  UK9 S1    UJ12 S1  UM6 I2b
; 16  UM5 D0   MC16  UM5 Q7  PL16 AM2909 UK7 D0  UK9 S0    UJ12 S0  UM6 I3b


;;;
;;; 28:27 STK_CTL 
;;;

; Bit PROM     Label Latch
; 28  UL3 D4   MC28  UL5 Q4  PL28 AM2909 UK7 PUP
; 27  UL3 D3   MC27  UL5 Q3  PL27 ~~(PL27 ^ JSR?) >= AM2909 UK7 ~FE


;;;
;;; 54 32:29 SEQ_CTL 
;;;

; Bit PROM     Label Latch
; 54  UK3 D6  MC55  UK5 Q1  PL55  ~(~(PL54 ^ ~PL32) ^ /JSR?) >= AM2909 UJ7 S1
; 32  UH3 D3  MC32  UH5 Q7  PL32  ~(PL32 ^ /JSR?) >= AM2911 UL9 S1
; 31  UL3 D7  MC31  UL5 Q4  PL31  ~(PL31 ^ /JSR?) >= AM2909 UJ7 S0, AM211 UL9 S0
; 30  UL3 D6  MC38  UL5 Q1  PL30  ~(PL30 ^ /JSR?) >= AM2909 UK7 S1
; 29  UL3 D5  MC29  UL5 Q5  PL29  ~(PL29 ^ /JSR?) >= AM2909 UK7 S0

; If /JSR? not asserted...

; PL29  PL30 ==>  UK7  S0 S1
;    0     0             1  1   microcode counter
;    0     1             1  0   stack
;    1     0             0  1   register
;    1     1             0  0   direct inputs


def SEQ_ALUL_uPC NO_JSR SEQ_CTL<=0bxxx00
def SEQ_ALUL_REG NO_JSR SEQ_CTL<=0bxxx01
def SEQ_ALUL_STK NO_JSR SEQ_CTL<=0bxxx10
def SEQ_ALUL_DTA NO_JSR SEQ_CTL<=0bxxx11

def SEQ_ALUHX_uPC NO_JSR SEQ_CTL<=0b100xx
def SEQ_ALUHX_REG NO_JSR SEQ_CTL<=0b101xx
def SEQ_ALUHX_STK NO_JSR SEQ_CTL<=0b110xx
def SEQ_ALUHX_DTA NO_JSR SEQ_CTL<=0b111xx

; All ALUs sequencers set to uPC
def SEQ_ALU_uPC NO_JSR SEQ_CTL<=0b10000  ; All sequencers set to uPC <-- Data
def SEQ_ALU_CONTINUE NO_JSR SEQ_CTL<=0b11111  ; All sequencers set to uPC ++
def SEQ_ALU_END_LOOP NO_JSR SEQ_CTL<=0b10101  ; All sequencers set to pop stack, use AR for address
 
; UL9 ALU ext.bits is (~PL31, ~PL32)


; 0000
;  31 0         ~(PL31 ^ /JSR?)
;               ~(0 ^ 1)
;               ~(0)
;               1 => UJ7 S0
;  54 0 32 0 => ~(~(PL54 ^ ~PL32) ^ 1)
;               ~(~(0 ^ ~0) ^ 1)
;               ~(~(0) ^ 1)
;               ~(1 ^ 1)
;               ~(1)
;               0  => UJ7 S1
;  U77 microprogram counter
;
;  29 0     =>  ~(PL29 ^ /JSR?)
;               ~(0 ^ 1)
;               ~(0)
;               1  >= UK7 S0
;  29 0     =>  ~(PL30 ^ /JSR?)
;               ~(0 ^ 1)
;               ~(0)
;               1  >= UK7 S0

;;;
;;; 33 CND_ENB   Microcode conditionals
;;;

; Bit PROM     Label Latch
; 33  UH3 D3  MC33  UH5 Q6  PL33  UJ13 Ea  UJ13 Eb  UK13 Ea  UK13 Eb


;;;
;;; 39:34 ALU
;;;

; Bit PROM     Label Latch
; 39 UH3 D0    MC39 UH5 Q0 ALU.I5  AM2901 UF9 I5  AM2901 UF7 I5
; 38 UH3 D1    MC38 UH5 Q1 ALU.I4  AM2901 UF9 I4  AM2901 UF7 I4
; 37 UH3 D5    MC37 UH5 Q5 ALU.I3  AM2901 UF9 I3  AM2901 UF7 I3
; 36 UH3 D4    MC36 UH5 Q4 ALU.I2  AM2901 UF9 I2  AM2901 UF7 I2
; 35 UH3 D3    MC35 UH5 Q3 ALU.I1  AM2901 UF9 I1  AM2901 UF7 I1
; 34 UH3 D2    MC34 UH5 Q2 ALU.I0  AM2901 UF9 I0  AM2901 UF7 I0

; A: data at memory[address on data pins A3:A0]
; B: data at memory[address on data pins B3:B0]
; D: data on pins D3:d0
; Q: Q register
; 0: zero

def ALU_A+Q ALU<=0b000000  ; A + Q + Cn
def ALU_A+B ALU<=0b000001  ; A + B + Cn
def ALU_0+Q ALU<=0b000010  ; 0 + Q + Cn
def ALU_0+B ALU<=0b000011  ; 0 + B + Cn
def ALU_0+A ALU<=0b000100  ; 0 + A + Cn
def ALU_D+A ALU<=0b000101  ; D + A + Cn
def ALU_D+Q ALU<=0b000110  ; D + Q + Cn
def ALU_D+0 ALU<=0b000111  ; D + 0 + Cn

def ALU_Q-A ALU<=0b001000  ; Q - A - ~Cn
def ALU_B-A ALU<=0b001001  ; B - A - ~Cn
def ALU_Q-0 ALU<=0b001010  ; Q - 0 - ~Cn
def ALU_B-0 ALU<=0b001011  ; B - 0 - ~Cn
def ALU_A-0 ALU<=0b001100  ; A - 0 - ~Cn
def ALU_A-D ALU<=0b001101  ; A - D - ~Cn
def ALU_Q-D ALU<=0b001110  ; Q - D - ~Cn
def ALU_0-D ALU<=0b001111  ; 0 - D - ~Cn

def ALU_A-Q ALU<=0b010000  ; A - Q - ~Cn
def ALU_A-B ALU<=0b010001  ; A - B - ~Cn
def ALU_0-Q ALU<=0b010010  ; 0 - Q - ~Cn
def ALU_0-B ALU<=0b010011  ; 0 - B - ~Cn
def ALU_0-A ALU<=0b010100  ; 0 - A - ~Cn
def ALU_D-A ALU<=0b010101  ; D - A - ~Cn
def ALU_D-Q ALU<=0b010110  ; D - Q - ~Cn
def ALU_D-0 ALU<=0b010111  ; D - 0 - ~Cn

def ALU_A_OR_Q ALU<=0b011000  ; A OR Q
def ALU_A_OR_B ALU<=0b011001  ; A OR B
def ALU_0_OR_Q ALU<=0b011010  ; 0 OR Q
def ALU_0_OR_B ALU<=0b011011  ; 0 OR B
def ALU_0_OR_A ALU<=0b011100  ; 0 OR A
def ALU_D_OR_A ALU<=0b011101  ; D OR A
def ALU_D_OR_Q ALU<=0b011110  ; D OR Q
def ALU_D_OR_0 ALU<=0b011111  ; D OR 0

def ALU_A_AND_Q ALU<=0b100000  ; A AND Q
def ALU_A_AND_B ALU<=0b100001  ; A AND B
def ALU_0_AND_Q ALU<=0b100010  ; 0 AND Q
def ALU_0_AND_B ALU<=0b100011  ; 0 AND B
def ALU_0_AND_A ALU<=0b100100  ; 0 AND A
def ALU_D_AND_A ALU<=0b100101  ; D AND A
def ALU_D_AND_Q ALU<=0b100110  ; D AND Q
def ALU_D_AND_0 ALU<=0b100111  ; D OR 0

def ALU_/A_AND_Q ALU<=0b101000  ; ~A AND Q
def ALU_/A_AND_B ALU<=0b101001  ; ~A AND B
def ALU_/0_AND_Q ALU<=0b101010  ; ~0 AND Q
def ALU_/0_AND_B ALU<=0b101011  ; ~0 AND B
def ALU_/0_AND_A ALU<=0b101100  ; ~0 AND A
def ALU_/D_AND_A ALU<=0b101101  ; ~D AND A
def ALU_/D_AND_Q ALU<=0b101110  ; ~D AND Q
def ALU_/D_AND_0 ALU<=0b101111  ; ~D OR 0

def ALU_A_XOR_Q ALU<=0b110000  ; A XOR Q
def ALU_A_XOR_B ALU<=0b110001  ; A XOR B
def ALU_0_XOR_Q ALU<=0b110010  ; 0 XOR Q
def ALU_0_XOR_B ALU<=0b110011  ; 0 XOR B
def ALU_0_XOR_A ALU<=0b110100  ; 0 XOR A
def ALU_D_XOR_A ALU<=0b110101  ; D XOR A
def ALU_D_XOR_Q ALU<=0b110110  ; D XOR Q
def ALU_D_XOR_0 ALU<=0b110111  ; D XOR 0

def ALU_A_/XOR_Q ALU<=0b111000  ; A ~XOR Q
def ALU_A_/XOR_B ALU<=0b111001  ; A ~XOR B
def ALU_0_/XOR_Q ALU<=0b111010  ; 0 ~XOR Q
def ALU_0_/XOR_B ALU<=0b111011  ; 0 ~XOR B
def ALU_0_/XOR_A ALU<=0b111100  ; 0 ~XOR A
def ALU_D_/XOR_A ALU<=0b111101  ; D ~XOR A
def ALU_D_/XOR_Q ALU<=0b111110  ; D ~XOR Q
def ALU_D_/XOR_0 ALU<=0b111111  ; D ~XOR 0

;;;
;;; 42:40 ALU_DST 
;;;

; Bit PROM     Label Latch
; 42  UF3 D2   MC42  UF5 Q2 ALU.I8  AM2901 UF9 I8  AM2901 UF7 I8
; 41  UF3 D1   MC41  UF5 Q6 ALU.I7  AM2901 UF9 I7  AM2901 UF7 I7  UH6 OEa  ~ALU.I7 => OEb
; 40  UF3 D0   MC40  UF5 Q7 ALU.I6  AM2901 UF9 I6  AM2901 UF7 I6


;;;
;;; 46:43 ARB_SEL 
;;;

; UF11 controlled by ~STATE1.EN, UM13 by ~STATE2.EN

; Bit PROM     Label Latch
; 46  UF3 D6   MC46  UF5 Q1 ALU.B3  AM2901 UF9 B3  AM2901 UF7 B3  UF11 A2  UM13 A2
; 45  UF3 D5   MC45  UF5 Q5 ALU.B2  AM2901 UF9 B2  AM2901 UF7 B2  UF11 A1  UM13 A1
; 44  UF3 D4   MC44  UF5 Q4 ALU.B1  AM2901 UF9 B1  AM2901 UF7 B1  UF11 A0  UM13 A0
; 43  UF3 D3   MC43  UF5 Q3 ALU.B0  AM2901 UF9 B0  AM2901 UF7 B0  UF11 D   UM13 D

;;;
;;; 50:47 ARA_SEL 
;;;

; Bit PROM     Label Latch
; 50  UK3 D2   MC59  UK5 Q2 ALU.A3  AM2901 UF9 A3  AM2901 UF7 A3
; 49  UK3 D1   MC49  UK5 Q6 ALU.A2  AM2901 UF9 A2  AM2901 UF7 A2
; 48  UK3 D0   MC48  UK5 Q7 ALU.A1  AM2901 UF9 A1  AM2901 UF7 A1
; 47  UF3 D7   MC47  UF5 Q0 ALU.A0  AM2901 UF9 A0  AM2901 UF7 A0


;;;
;;; 52:51 SFT_CRY 
;;;

; Bit PROM     Label Latch
; 52  UK3 D4   MC52  UK5 D4 PL52   UF6 S1  UH6 S1
; 51  UK3 D3   MC51  UK5 D3 PL51   UF6 S0  UH6 S0


;;;
;;; 53 RLO 
;;;

; Bit PROM     Label Latch
; 53  UK3 D5   MC53  UK5 D5 PL53 

; Controls whether high or low byte is read/written during register access

;;;
;;; INT_LVL 55
;;;

; Bit PROM     Label Latch
; 55  UK3 D7   MC55  UK5 D0 PL55   UC14 S

def USE_REGIDX INT_LVL<=0b0  ; Use register index for register access
def USE_IPL  INT_LVL<=0b1  ; Use IPL for register access

def default USE_REGIDX NO_JSR


