uasm: A quick-and-dirty microcode assembler for Centurion CPU6

Usage:  uasm <name>
  Assembles <name>.uasm to <name>.rom, plus a listing file to <name>.lst

TODO:
  Detailed listing
  Complex field definitions

Source format:

   Semicolons mark comments; text from the semicolon to the end of line is 
   discarded.

   Name character set:

       0123456789
       ABCDEFGHIJKLMNOPQRSTUVWXYZ
       abcdefghijklmnopqrstuvwxyz
       #$%+.-/@_'`

   Labels are defined with a colon:

     ; Label the current ROM word as "FOO"
     : FOO


   Field definitions:

     "field" <name> ["~"] <field_list> "."

     <field_list> :=  <field> [ <field_list> ]
     <field> :=  ["~"]  <number> |
                  <number> ":" <number>

     ; Define a field name "STK_CTL" in the instruction word from bits 28 to 27
     field STK_CTL 28 : 27.
     ; Define a screwy field
     ; X 16 8 X X X 1 2 X 4  
     field WIERD 8 : 7  0  2  3

   Fields can have named values associated

     field INT_LVL 55
     tag INT_LVL   INT_LVL_0 0b0  INT_LVL_1 0b1
     INT_LVL<=INT_LVL_0
     INT_LVL<=INT_LVL_1
     field FOO 54
     FOO<=INT_LVL_0  ; generates an error as INT_LVL_0 is for field INT_LVL
 
   Field setting:

     ; Set bits 28 and 27 to 1
     STK_CTL = 0b11

   Write word to ROM

     ; <Exclamation point> writes the current instruction to ROM and advances
     ;  to the next address
     !

     Example: The first word of microcode from CodeROM.txt

         ; 42abc618b781c0
         INT_LVL = 0b0  SEQ_CTL0 = 0b1  RLO = 0b0  SFT_CRY = 0b00  
         ARA_SEL = 0b0101  ARB_SEL = 0b0101  ALU_DST = 0b011  
         ALU_SRC = 0b110001  CND_ENB = 0b1  SEQ_CTL1 = 0b0000  
         STK_CTL = 0b11  SEQ_BITS = 0b00010110111  CND_JSR = 0b1  
         CNTRL0 = 0b00  CNTRL1 = 0b000  REG_LD0 = 0b011  REG_LD1 = 0b100  
         BUS_SEL <= 0b0000  ! ; 000

   Macros

     Definition:
       def <name> <body to end of line>
       def FOO BAR BAZ

     Usage: Any appearence of <name> will be replaced with the macro body. Macro
     references inside a macro body will will be expanded.
     
     Example:
       The disassembler generates
          field INT_LVL 55
       and each uinstruction starts
          INT_LVL = 0b0  
       or
          INT_LVL = 0b1

       This can be replaced with:

          def INT0 INT_LVL = 0b0
          def INT1 INT_LVL = 0b1

       and changing the uinstruction starts to

          INT0
        or
          INT1

    Include files

       include file_name

    Disassmbly command

       "disasm filename field_list"
       Creates a disassembly of CodeROM based on the fields (and their tags) listed in field_list.

    Parser is simple and assumes space delimited filenames, so no embedded white space.

The object file format is the same as CodeROM.txt.

Building:

    make

    This will build uasm (the assembler) and udisasm (the disassembler). The 
    code is written in C and should build anywhere the C compiler supports 
    uint64_t.


Running:

    "make test" will run the disassembler on CodeROM.txt producing test.uasm.
    It will then assemble test.uasm into test.rom and compare that with
    CodeROM.txt to demonstrate assembler correctness.

    "make CodeROM" will run the disassembler on CodeROM.txt producing
    CodeROM.uasm. CodeROM.uasm will the start point for reverse engineering
    the microcode.

Manifest
    CodeROM.txt   the CPU6 microcode
    Makefile
    README.txt    this file
    uasm.c        source for the assembler
    udisasm.c     source for the disassembler


