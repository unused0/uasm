all : uasm .CodeROM.uasm CodeROM.pdf


.CodeROM.uasm : uasm CodeROM.uasm
	cp CodeROM.uasm CodeROM.uasm.$(shell date --iso=seconds)
	cp CodeROM.latex CodeROM.latex.$(shell date --iso=seconds)
	./uasm CodeROM
	touch -r CodeROM.uasm .CodeROM.uasm

CodeROM.pdf : .CodeROM.uasm CodeROM.latex
	pdflatex CodeROM.latex

uasm : uasm.c
	gcc uasm.c -o uasm -Wall -O0 -g3

clean :
	-rm uasm CodeROM.lst CodeROM.log CodeROM.pdf CodeROM.aux 


